/////////////////////////////////////////////////////////////////////////////
///
/// @file SRT_Event_FIFO.cpp
///
/// @brief File containing classes which enable communication between tasks.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"
#include "Application.h"
#include "SRT_Event_FIFO.h"
#include "Exception_Manager.h"



/////////////////////////////////////////////////////////////////////////////
//
// FIFO_uChar_Buffer - Member functions
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
///
/// FIFO_uChar_Buffer - Constructor for FIFO which initializes the variables
///
/////////////////////////////////////////////////////////////////////////////

c_srt_event_fifo_buffer::c_srt_event_fifo_buffer()
{
	FIFO_Finish = 0;
	FIFO_Length = 0;
	FIFO_Max_Length = 0;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Length - Function that returns number of items in the buffer
///
/// @return Number of items in the buffer
///
/////////////////////////////////////////////////////////////////////////////

uint16_t c_srt_event_fifo_buffer::Length ( void )
{
	return FIFO_Length;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Max_Length - Function that returns maximum number of items ever in the
/// buffer
///
/// @return Maximum number of items ever in the buffer
///
/////////////////////////////////////////////////////////////////////////////

uint16_t c_srt_event_fifo_buffer::Max_Length ( void )
{

	return FIFO_Max_Length;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Push - Function that adds an item to the buffer
///
/// @param [in] Item - Item to be added
///
/// Processing as follows:
///
/////////////////////////////////////////////////////////////////////////////

void c_srt_event_fifo_buffer::Push ( t_srt_events Item )
{
	int16_t Queue_Start; // Position within buffer to store new item

	/// + On detection that buffer is full, function will record the
	/// exception and fail gracefully by loosing the data
	if ( FIFO_Length == SRT_EVENT_FIFO_LENGTH )
	{
		Exception_Handler ( EXC_SRT_FIFO_FULL, EXC_TYPE_NON_CRITICAL );
		return;
	}

	/// + Disable global int16_terrupts
	__disable_irq();

	/// + Calculate the position of the start of the queue
	Queue_Start = FIFO_Finish + FIFO_Length;

	/// + Loop event position when end of array is exceeded
	if ( Queue_Start >= SRT_EVENT_FIFO_LENGTH )
	{
		Queue_Start = Queue_Start - SRT_EVENT_FIFO_LENGTH;
	}

	/// + Store data within buffer
	FIFO_Buffer_Store[ Queue_Start ] = Item;

	/// + Increment queue length
	FIFO_Length = FIFO_Length + 1;

	/// + Update record of maximum queue length if it has been exceeded
	if ( FIFO_Length > FIFO_Max_Length )
	{
		FIFO_Max_Length = FIFO_Length;
	}

	/// + Enable global int16_terrupts
	__enable_irq();
}


/////////////////////////////////////////////////////////////////////////////
///
/// Pull - Function that removes an item from the buffer
///
/// @return Data to removed from buffer
///
/// Processing as follows:
///
/////////////////////////////////////////////////////////////////////////////

t_srt_events c_srt_event_fifo_buffer::Pull ( void )
{
	t_srt_events Data; // Item to be removed from buffer

	/// + On detection that buffer is empty, function will record the
	/// exception and returning zero for the data
	if ( FIFO_Length == 0 )
	{
		Exception_Handler ( EXC_SRT_FIFO_EMPTY, EXC_TYPE_NON_CRITICAL );
		return SRT_EVENT_NULL;
	}

	/// + Disable global int16_terrupts
	__disable_irq();

	/// + Load received data
	Data = FIFO_Buffer_Store[ FIFO_Finish ];

	/// + Increment oldest event position within buffer
	FIFO_Finish = FIFO_Finish + 1;

	/// + Loop event position when end of array is exceeded
	if ( FIFO_Finish == SRT_EVENT_FIFO_LENGTH )
	{
		FIFO_Finish = 0;
	}

	/// + Decrement event queue length
	FIFO_Length = FIFO_Length - 1;

	/// + Enable global int16_terrupts
	__enable_irq();

	/// + Return data
	return Data;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Reset_Max - Function that resets the Max_Length variable
///
/////////////////////////////////////////////////////////////////////////////

void c_srt_event_fifo_buffer::Reset_Max ( void )
{
	FIFO_Max_Length = FIFO_Length;
}