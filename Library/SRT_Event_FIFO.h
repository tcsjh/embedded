/////////////////////////////////////////////////////////////////////////////
///
/// @file SRT_Event_FIFO.h
///
/// @brief File containing headers for classes which enable communication
/// between tasks.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Uppercase URL UID created by www.guidgenerator.com
#ifndef SRT_EVENT_FIFO_H_81B9AD50EABA4617B7BD51CEDF91A15D
#define SRT_EVENT_FIFO_H_81B9AD50EABA4617B7BD51CEDF91A15D


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "Application.h"


// Function prototypes

/////////////////////////////////////////////////////////////////////////////
///
/// c_srt_event_fifo_buffer - Class for First In First Out (FIFO) Buffer for Char
///
/// See SRT_Events.h for Node specific configuration data
///
/////////////////////////////////////////////////////////////////////////////

class c_srt_event_fifo_buffer
{
	uint16_t FIFO_Finish; ///< Location of oldest data item in the buffer
	uint16_t FIFO_Length; ///< Number of items in buffer
	uint16_t FIFO_Max_Length; ///< Maximum number of items ever in buffer
	t_srt_events FIFO_Buffer_Store[ SRT_EVENT_FIFO_LENGTH ]; ///< Array to hold buffer
public:
	c_srt_event_fifo_buffer();
	uint16_t Length ( void );
	uint16_t Max_Length ( void );
	void Push ( t_srt_events );
	t_srt_events Pull ( void );
	void Reset_Max ( void );
};


#endif // SRT_EVENT_FIFO_H_81B9AD50EABA4617B7BD51CEDF91A15D