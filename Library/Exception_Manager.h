/////////////////////////////////////////////////////////////////////////////
///
/// @file Exception_Manager.h
///
/// @brief File containing containing headers for managing exception events.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Uppercase URL UID created by www.guidgenerator.com
#ifndef EXCEPTION_MANAGER_H_61EA6979CB1144B29391301D1FFCDE6F
#define EXCEPTION_MANAGER_H_61EA6979CB1144B29391301D1FFCDE6F


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "Application.h"


/////////////////////////////////////////////////////////////////////////////
//
// Exception Event Classifications
//
/////////////////////////////////////////////////////////////////////////////

/// List of all possible exception classifications
typedef enum
{
	EXC_TYPE_CRITICAL, ///< Exception which should recorded and the system held in shut down
	EXC_TYPE_RESET, ///< Exception which should be recorded and system reset initiated
	EXC_TYPE_NON_CRITICAL ///< Exception which should be recorded and system continue in operation

} t_exception_types;


// Function prototypes

void Exception_Handler ( t_node_exceptions Exception_ID, t_exception_types Exception_Type );


#endif // EXCEPTION_MANAGER_H_61EA6979CB1144B29391301D1FFCDE6F