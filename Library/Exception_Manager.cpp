/////////////////////////////////////////////////////////////////////////////
///
/// @file Exception_Manager.cpp
///
/// @brief File containing functions for managing exception events.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"
#include "Application.h"
#include "Node_Drivers.h"
#include "Exception_Manager.h"
#include "NVM_Drivers.h"
#include "Node_Data_Access.h"
#include "WDT_Drivers.h"



/////////////////////////////////////////////////////////////////////////////
///
/// Exception_Handler - Function for the capturing, recording and management
/// of exception conditions.
///
/// See Node Exceptions.h for Node specific exception data
///
/// @param [in] Exception_ID - Identifier for last exception
/// @param [in] Exception_Type - Classification of exception severity
///
/////////////////////////////////////////////////////////////////////////////

void Exception_Handler ( t_node_exceptions Exception_ID, t_exception_types Exception_Type )
{
	// Exception reset
	if ( Exception_ID == EXC_NO_EXCEPTION )
	{
		ND_Write ( ND_EXCEPTION_STATUS, EXC_NO_EXCEPTION );
		NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_EXCEPTION_STATUS ), ND_Read ( ND_EXCEPTION_STATUS ) );
		CAN0_Red_LED_Clear();
		return;
	}

	// If reported exception is worse than any previous exceptions raise
	// and store exception status
	if ( Exception_ID > ND_Read ( ND_EXCEPTION_STATUS ) )
	{
		ND_Write ( ND_EXCEPTION_STATUS, Exception_ID );
		NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_EXCEPTION_STATUS ), ND_Read ( ND_EXCEPTION_STATUS ) );
	}

	// Illuminate exception LED
	CAN0_Red_LED_Set();

	// Manage exception based on severity
	switch ( Exception_Type )
	{
		/// Exception which should recorded and the system held in shut down
		case EXC_TYPE_CRITICAL:

			while ( true )
			{
				// Clear WDT
				WDT_Clear();
			}
			break;

		/// Exception which should be recorded and system reset initiated
		case EXC_TYPE_RESET:

			// WDT will be triggered causing a lower priority exception
			while ( true );
			break;

		/// Exception which should be recorded and system continue in operation
		case EXC_TYPE_NON_CRITICAL:
			break;
	}
}