### Hardware Embedded - Overview

This repository contains embedded software for the Atmel C21/E51 microcontrollers which has been developed and applied to a number of applications.

The core architecture has been based on the producer-consumer real time software architecture model favoured by LabView.

The libraries are fully documented using doxygen, the doxygen run file may be found under ../doxygen/doxyfile.

These libraries were originally developed by Chris Wheater before joining TCS John Huxley (copyright waved, background IP retained), and have been copied from libraries originally written to run on Microchip PIC Microcontrollers.