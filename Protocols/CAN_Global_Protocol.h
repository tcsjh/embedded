#ifndef INC_CAN_GLOBAL_PROTOCOL_H_A7A5A798893544D99D6FE8E4890129CB
#define INC_CAN_GLOBAL_PROTOCOL_H_A7A5A798893544D99D6FE8E4890129CB
/////////////////////////////////////////////////////////////////////////////
///
/// @file CAN_Global_Protocol.h
///
/// @brief File containing the global CAN protocol messages all for nodes on
/// this CAN data bus.
///
/// The protocol uses the least significant 5 bits of the message identifier
/// to contain the Node Identifier and the most significant 6 bits of the
/// message identifier to contain the Message Identifier for that message
/// from that particular Node Type.  This enables up to 32 different Nodes
/// to be on the bus, each of which has 64 unique message types for each
/// Node Type.
///
/// @image html CAN_Frame.jpg
///
/// @image html Message_Identifier.jpg
///
/// Two Message Identifiers have been reserved for all Node Types.
///
/// Message ID 0b00000 (the highest priority messages) have been reserved for
/// any node to raise a request for a system shutdown (when the data length
/// is zero), or to broadcast an exception event.
///
/// Message ID 0b11111 (the lowest priority messages) have been reserved for
/// management of the bus, including:  the broadcast of heartbeats and broken
/// heartbeats by each node; roll call, which requests all nodes on the bus
/// to identify themselves; assigning Node IDs; and requests and broadcasting
/// of test data.
///
/// The protocol is reliant on each node being allocated a Node Type (SW ID) and
/// Serial Number at the point of manufacture, then the correct Node
/// Identifier before operating within the application.
///
/// Node ID 0 has been reserved for the node types which can manage the
/// allocation of node Serial Numbers, Node Identifiers, and request test
/// data.
///
/// The management of node identifiers must take the following sequence:
/// + During development a unique Node Software ID and Issue shall be allocated
/// and hard coded within the node software.
/// + During manufacture the node needs to be put on a CAN data bus with no
/// other node apart from the CAN Serial Number Programmer.  The CAN Serial
/// Number Programmer shall broadcast a ROLL_CALL_RESPONSE to determine the
/// initial details of the node to be allocated a serial number.  The node then
/// can be allocated a unique Serial Number and informed of its hardware build
/// issue, the node will then have a unique Node SW ID and Node Serial Number
/// combination.
/// + During installation the node needs to be put on a CAN data bus with no
/// other nodes apart from the CAN Configurator.  The CAN Configurator shall
/// broadcast a ROLL_CALL_RESPONSE to determine what nodes are on the CAN
/// data bus and their Node SW ID and Serial Number.  The CAN Configurator
/// shall use this data to assign the node a new Node ID via the
/// NODE_ID_ASSIGN message.
/// + During operation each of the nodes will have been provided with unique
/// Node ID for that CAN data bus to prevent message clashes and enable
/// communications in accordance with the design of that application
/// protocol.  All nodes will broadcast heartbeats at 1Hz, and broadcast and
/// monitor for exception messages.  When nodes detect an exception they
/// broadcast broken heartbeats instead.  The CAN Control will use these and
/// other messages to monitor and configure the system.
///
/// @copyright Confidential.  Copyright (C) 2020-2021, by Chris Wheater
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// CAN Protocol - 500kbs, Standard CAN Messages
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// Global Message Format
//
/////////////////////////////////////////////////////////////////////////////

#define CAN_MESSAGE_POS 5 ///< Position within the CAN ID for message ID
#define CAN_ID_MASK 0b00000011111 ///< Mask for enabling any message from a specific CAN Node ID
#define CAN_ID_MAX 31 ///< Maximum value of a CAN Node ID
#define CAN_MSG_MASK 0b11111100000 ///< Mask for enabling a specific message from any CAN Node ID
#define CAN_ID_SYSTEM_CONTROLLER 0; ///< CAN Node ID for System Controllers


/////////////////////////////////////////////////////////////////////////////
//
// CAN Bus Global Message Data
//
/////////////////////////////////////////////////////////////////////////////

/// CAN_MSG_EXCEPTION sub-messages
///
/// Message ID 0b00000 (the highest priority messages) have been reserved for
/// any node to raise a request for a system shutdown (when the data length
/// is zero), or to broadcast an exception event.
///
/// | Message Name      | Sub-Message Name      | Message ID | Node ID | Length             | DB0            | DB1            | DB2            | DB3            | DB4                  | DB5                  | DB6                 | DB7                 | Description |
/// | :---------------: | :-------------------: | :--------: | :-----: | :----------------: |:-------------: | :------------: | :------------: | :------------: | :------------------: | :------------------: | :-----------------: | :-----------------: | :----------------------------: |
/// | CAN_MSG_EXCEPTION | SHUTDOWN              | 000000     | NODE_ID | 0                  |                |                |                |                |                      |                      |                     |                     | Transmitted by any node to all nodes, requiring them to adopt safe modes and shut down.  No data |
/// | CAN_MSG_EXCEPTION | EXCEPTION             | 000000     | NODE_ID | Node Type Specific | Exception Data | Exception Data | Exception Data | Exception Data | Exception Data       | Exception Data       | Exception Data      | Exception Data      | Transmitted by all nodes, flagging an exception condition and any supporting data which is node specific |

#define CAN_MSG_EXCEPTION 0b000000

/// See table
enum CAN_MSG_Exception_Submessages
{
	CAN_MSG_EXCEPTION_SHUTDOWN, ///< See table
	CAN_MSG_EXCEPTION_EXCEPTION ///< See table
};


/// CAN_MSG_PROTOCOL sub-messages
///
/// Message ID 0b11111 (the lowest priority messages) have been reserved for
/// management of the bus, including:  the broadcast of heartbeats and broken
/// heartbeats by each node; roll call, which requests all nodes on the bus
/// to identify themselves; assigning Node IDs; and requests and broadcasting
/// of test data.
///
/// Two Exception ID are common to all nodes types, see Node_Exceptions.h for other exceptions for that node:*
/// + 0 - No exception
/// + 1 - System shutdown rebroadcast
///
/// | Message Name      | Sub-Message Name        | Message ID | Node ID | Length             | DB0            | DB1            | DB2                 | DB3                 | DB4                  | DB5                  | DB6                 | DB7                 | Description |
/// | :---------------: | :---------------------: | :--------: | :-----: | :----------------: | :------------: | :------------: | :-----------------: | :-----------------: | :------------------: | :------------------: | :-----------------: | :-----------------: | :----------------------------: |
/// | CAN_MSG_PROTOCOL  | HEARTBEAT               | 111111     | NODE_ID | 0                  |                |                |                     |                     |                      |                      |                     |                     | Transmitted every second by all nodes. No data |
/// | CAN_MSG_PROTOCOL  | BROKEN_HEARTBEAT        | 111111     | NODE_ID | Node Type Specific | 1              | Exception ID*  | Exception Data      | Exception Data      | Exception Data       | Exception Data       | Exception Data      | Exception Data      | Transmitted every second by nodes with exception conditions in place of normal heartbeat |
/// | CAN_MSG_PROTOCOL  | ROLL_CALL_RESPONSE      | 111111     | NODE_ID | 6                  | 2              | 0              | Node SW ID LSB      | Node SW ID MSB      | Node Serial No. LSB  | Node Serial No. MSB  |                     |                     | Transmitted by all nodes in response to Roll Call Request |
/// | CAN_MSG_PROTOCOL  | TEST_DATA_RESPONSE      | 111111     | NODE_ID | 8                  | 3              | 0              | Data ID             | 0                   | Test Data LSB        | Test Data MSB        | Data channel        |                     | Transmitted by all nodes in response to Test Data Request |
/// | CAN_MSG_PROTOCOL  | BUILD_STD_RESPONSE      | 111111     | NODE_ID | 8                  | 4              | 0              | Node SW ID LSB      | Node SW ID MSB      | Node SW Issue LSB    | Node SW Issue MSB    | Node HW Issue LSB   | Node HW Issue MSB   | Transmitted by requested node in response to Build Standard of a Node from CAN Controller |
/// | CAN_MSG_PROTOCOL  | ROLL_CALL_REQUEST       | 111111     | 00000   | 2                  | 5              | 0              |                     |                     |                      |                      |                     |                     | Transmitted by CAN Controller to instigate Roll Call |
/// | CAN_MSG_PROTOCOL  | HEARTBEAT_REQUEST       | 111111     | 00000   | 4                  | 6              | Node ID        | 0(off)/1(on)        | 0                   |                      |                      |                     |                     | Transmitted by CAN Controller to enable disable heartbeat from node |
/// | CAN_MSG_PROTOCOL  | NODE_ID_ASSIGN          | 111111     | 00000   | 8                  | 7              | Node ID        | Node Type LSB       | Node Type MSB       | Node Serial No. LSB  | Node Serial No. MSB  | New Node ID         | 0                   | Transmitted by CAN Controller to assign a Node ID to the node with the specified by type and serial number, and ignored otherwise |
/// | CAN_MSG_PROTOCOL  | TEST_DATA_REQUEST       | 111111     | 00000   | 8                  | 8              | Node ID        | Data ID             | 0                   | Data Tx int16_terval LSB | Data Tx int16_terval MSB | Data channel        | 0                   | Transmitted by CAN Controller to request test data.  The interval defines the repeat period in mS, zero indicating a one off transmission.  Data type is node type specific and held within the node's Application.h.  Each node can transmit 4 data items at the same time on channels 0 to 3.  |
/// | CAN_MSG_PROTOCOL  | MIN_MAX_RESET           | 111111     | 00000   | 2                  | 9              | Node ID        |                     |                     |                      |                      |                     |                     | Transmitted by CAN Controller to request reset of min/max of test data from current power sequence. |
/// | CAN_MSG_PROTOCOL  | SERIAL_NO_ASSIGN        | 111111     | 00000   | 6                  | 10             | Node ID        | Node Serial No. LSB | Node Serial No. MSB | Node HW Issue LSB    | Node HW Issue MSB    |                     |                     | Transmitted by CAN Controller to assign serial number at point of manufacture (manufacturing controllers only) |
/// | CAN_MSG_PROTOCOL  | BUILD_STD_REQUEST       | 111111     | 00000   | 2                  | 11             | Node ID        |                     |                     |                      |                      |                     |                     | Transmitted by CAN Controller to request Build Standard of a Node |
/// | CAN_MSG_PROTOCOL  | EXCEPTION_RESET_REQUEST | 111111     | 00000   | 2                  | 12             | Node ID        |                     |                     |                      |                      |                     |                     | Transmitted by CAN Controller to request reset of exception condition of a Node |

#define CAN_MSG_PROTOCOL 0b111111

/// See table
enum CAN_MSG_Protocol_Submessages
{
	CAN_MSG_PROTOCOL_HEARTBEAT, ///< See table
	CAN_MSG_PROTOCOL_BROKEN_HEARTBEAT, ///< See table
	CAN_MSG_PROTOCOL_ROLL_CALL_RESPONSE, ///< See table
	CAN_MSG_PROTOCOL_TEST_DATA_RESPONSE, ///< See table
	CAN_MSG_PROTOCOL_BUILD_STD_RESPONSE, ///< See table
	CAN_MSG_PROTOCOL_ROLL_CALL_REQUEST, ///< See table
	CAN_MSG_PROTOCOL_HEARTBEAT_REQUEST, ///< See table
	CAN_MSG_PROTOCOL_NODE_ID_ASSIGN, ///< See table
	CAN_MSG_PROTOCOL_TEST_DATA_REQUEST, ///< See table
	CAN_MSG_PROTOCOL_MIN_MAX_RESET, ///< See table
	CAN_MSG_PROTOCOL_SERIAL_NO_ASSIGN, ///< See table
	CAN_MSG_PROTOCOL_BUILD_STD_REQUEST, ///< See table
	CAN_MSG_PROTOCOL_EXCEPTION_RESET_REQUEST, ///< See table
};

#endif // INC_CAN_GLOBAL_PROTOCOL_H_A7A5A798893544D99D6FE8E4890129CB