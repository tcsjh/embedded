/////////////////////////////////////////////////////////////////////////////
///
/// @file Application.cpp
///
/// @brief File containing the application specific functions for the
/// application.  These functions could run on any Circuit Card Assembly.
/// The application is documented by doxygen and the Application.txt file.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

// System files
#include "sam.h"
#include "Application.h"

// Node Driver files
#include "CAN0_Drivers.h"
#include "CLK_Drivers.h"
#include "NVM_Drivers.h"
#include "RSTC_Drivers.h"
#include "SUPC_Drivers.h"
#include "TSENS_Drivers.h"
#include "WDT_Drivers.h"

// Application Drivers
#include "Node_Drivers.h"

// Library files
#include "SRT_Event_FIFO.h"
#include "Exception_Manager.h"
#include "Node_Data_Access.h"

// Protocols
#include "CAN_Global_Protocol.h"



/////////////////////////////////////////////////////////////////////////////
//
// Create global objects and variables
//
/////////////////////////////////////////////////////////////////////////////

// Objects
c_srt_event_fifo_buffer g_srt_event_queue; ///< Object through which events are passed to the SRT Task Manager

// Structures
typedef struct
{
	uint16_t timer_count; ///< Current counter value for this timer
	uint16_t timer_period; ///< Period for this timer in milliseconds
	uint8_t timer_repeat; ///< Specification if timer should repeat when completed (true yes)

} t_timer_event_data; ///< Structure which contains information which specifies each timer event and holds status

// Variables
t_timer_event_data g_timer_events[ SRT_EVENT_TIMER_END ]; ///< Array to hold data for each timer event



/////////////////////////////////////////////////////////////////////////////
///
/// main - Boot routine for the system
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

int main ( void )
{
	/// + Initialize hardware
	Initialize();

	/// + Enable global interrupts, thus starting the HRT tasks
	__enable_irq();

	/// + Run SRT Task Manager
	SRT_Task_Manager();
}


/////////////////////////////////////////////////////////////////////////////
///
/// Initialize - Function that initializes the micro-controller and target
/// hardware.
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void Initialize ( void )
{
	struct can_standard_message_filter_element sd_filter; ///< Temporary variable for holding CAN filter.  This variable shall be 'reset' using the can_get_standard_message_filter_element_default ( &sd_filter ); call prior to use, then discarded.
	uint8_t index = 0; ///< Variable to hold the index and loop value
	uint32_t temp_sum = 0; ///< Variable used to hold temperature filter

	/// + Prevent interrupts until system is fully initialized
	__disable_irq();

	/// + Determine if last reset was triggered by the Watchdog Timer
	if ( WDT_Reset() )
	{
		Exception_Handler ( EXC_WDT_RESET, EXC_TYPE_NON_CRITICAL );
	}

	/// + Initialize Clocks
	Clocks_Init();

	/// + Initialise Delay functions
	Delay_Init();

	/// + Initialize NVM
	NVM_Init();

	/// + Initialise Test Pins
	Test_Pin_Init();

	/// + Initialise CAN Pins
	CAN_Pin_Init();

	/// + Initialize CAN0 bus
	CAN0_Init();

	/// + Initialize Supply Controller
	SUPC_Init();

	/// + Initialise timer to provide HRT synchronous interrupts
	TC0_Init();

	/// + Initialize temperature sensing and measure filtered temperature
	TSEN_Init();
	for ( index = 0; index <= 0x3F ; index = index + 1 )
	{
		temp_sum = temp_sum + Temp_Read();
		Delay_us ( 100 );
	}
	temp_sum =	temp_sum / 0x3F;


	/// + Initialise Node Data based on if if this is the first time
	/// this code has been run or from memory if not
	if ( NVM_Read_uInt ( NVM_NODE_DATA + 2 * ND_INITIALISED ) != 0xA0A0 )
	{
		// Initialise global variables on first run
		for ( index = 0; index < ND_SIZE; index = index + 1 )
		{
			ND_Write ( ( t_node_data ) index, 0 );
		}

		ND_Write ( ND_INITIALISED, 0xA0A0 );
		ND_Write ( ND_CAN_NODE_ID , CAN_ID_MASK );
		ND_Write ( ND_HW_ISSUE, 0xFFFF );
		ND_Write ( ND_HEARTBEAT_STATUS, 1 );
		ND_Write ( ND_TEMP, ( uint16_t ) temp_sum );
		ND_Write ( ND_TEMP_MIN, ND_Read ( ND_TEMP ) );
		ND_Write ( ND_TEMP_MAX, ND_Read ( ND_TEMP ) );

		// Store global variables
		for ( index = 0; index < ND_SIZE; index = index + 1 )
		{
			NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * index ), ND_Read ( ( t_node_data ) index ) );
		}
	}
	else
	{
		// Load global variables
		for ( index = 0; index < ND_SIZE; index = index + 1 )
		{
			ND_Write ( ( t_node_data ) index, NVM_Read_uInt ( NVM_NODE_DATA + ( 2 * index ) ) );
		}
		ND_Write ( ND_TEMP, ( uint16_t ) temp_sum );
		ND_Write ( ND_ETI_LSB, NVM_Read_uInt ( NVM_BROWNOUT_PAGE ) );
		ND_Write ( ND_ETI_MSB, NVM_Read_uInt ( NVM_BROWNOUT_PAGE + 2 ) );
		ND_Write ( ND_TEMP_MAX, NVM_Read_uInt ( NVM_BROWNOUT_PAGE + 4 ) );
		ND_Write ( ND_TEMP_MIN, NVM_Read_uInt ( NVM_BROWNOUT_PAGE + 6 ) );
	}


	/// + Illuminate exception LED if exception is stored
	if ( ND_Read ( ND_EXCEPTION_STATUS ) )
	{
		CAN0_Red_LED_Set();
	}


	/// + Initialise event timers

	// Initialise temperature update timer to raise an event every 10s
	Timer_Set ( SRT_EVENT_TEMPERATURE_TIMER, 10000, true );

	// Initialise 1Hz tick
	Timer_Set ( SRT_EVENT_1HZ_TIMER, 1000, true );

	// Initialize test data timers
	if ( ND_Read ( ND_TEST_DATA_0_INTERVAL ) )
	{
		Timer_Set ( SRT_EVENT_CAN_TEST_DATA0_TIMER, ND_Read ( ND_TEST_DATA_0_INTERVAL ), true );
	}

	if ( ND_Read ( ND_TEST_DATA_1_INTERVAL ) )
	{
		Timer_Set ( SRT_EVENT_CAN_TEST_DATA1_TIMER, ND_Read ( ND_TEST_DATA_1_INTERVAL ), true );
	}

	if ( ND_Read ( ND_TEST_DATA_2_INTERVAL ) )
	{
		Timer_Set ( SRT_EVENT_CAN_TEST_DATA2_TIMER, ND_Read ( ND_TEST_DATA_2_INTERVAL ), true );
	}

	if ( ND_Read ( ND_TEST_DATA_3_INTERVAL ) )
	{
		Timer_Set ( SRT_EVENT_CAN_TEST_DATA3_TIMER, ND_Read ( ND_TEST_DATA_3_INTERVAL ), true );
	}


	/// + Configure CAN reception filters
	// Configure a filter to receive exception messages from any mode
	index = 0;
	can_get_standard_message_filter_element_default ( &sd_filter );
	sd_filter.S0.bit.SFID1 = CAN_MSG_EXCEPTION << CAN_MESSAGE_POS;
	sd_filter.S0.bit.SFID2 = CAN_MSG_MASK;
	CAN0_Set_Rx_Filter ( index, &sd_filter );

	// Configure a filter to receive all messages from the CAN Controller
	index = index + 1;
	can_get_standard_message_filter_element_default ( &sd_filter );
	sd_filter.S0.bit.SFID1 = CAN_ID_SYSTEM_CONTROLLER;
	sd_filter.S0.bit.SFID2 = CAN_ID_MASK;
	CAN0_Set_Rx_Filter ( index, &sd_filter );


	/// + Initialize Watchdog Timer
	WDT_Init();

} // Initialize



/////////////////////////////////////////////////////////////////////////////
///
/// SRT_Task_Manager - Soft Real Time (SRT) Task Manager
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void SRT_Task_Manager ( void )
{
	struct can_rx_element_fifo_0 rx_element_fifo_0; ///< Variable containing the last CAN Rx Message
	struct can_tx_element tx_element; ///< Temporary variable holding CAN Tx Message.  This variable shall be 'reset' using the can_get_tx_buffer_element_defaults ( &tx_element ); call prior to use, then discarded.
	uint8_t can_rx_node; ///< Variable containing Node ID of CAN received message
	uint16_t can_rx_message; ///< Variable containing Message ID of CAN received message

	/// + SRT core processing loop
	while ( true )
	{
		/// + Set SRT loop pin if enabled
		#ifdef SRT_LOOP
		STL_Loop_Pin_Set();
		#endif

		/// + Clear WDT
		WDT_Clear();

		// Clear SRT loop pin
		#ifdef SRT_LOOP
		STL_Loop_Pin_Clear();
		#endif


		/// + Check SRT Event Buffer status and respond if necessary
		if ( g_srt_event_queue.Length() )
		{
			/// + Respond to SRT event
			switch ( g_srt_event_queue.Pull() )
			{
				// Timer event for one second tick
				case SRT_EVENT_1HZ_TIMER:

					// Send heartbeat message if enabled
					if ( ND_Read ( ND_HEARTBEAT_STATUS ) )
					{
						// If an exception status exists, send broken heartbeat, otherwise
						// send the heartbeat message
						if ( ND_Read ( ND_EXCEPTION_STATUS ) )
						{
							can_get_tx_buffer_element_defaults ( &tx_element );
							tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
							tx_element.T1.bit.DLC = 2;
							tx_element.data[0] = CAN_MSG_PROTOCOL_BROKEN_HEARTBEAT;
							tx_element.data[1] = ( uint8_t ) ND_Read ( ND_EXCEPTION_STATUS );
							CAN0_Green_LED_Set();
							Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
							CAN0_Message_Tx ( &tx_element );
						}
						else
						{
							can_get_tx_buffer_element_defaults ( &tx_element );
							tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
							tx_element.T1.bit.DLC = 0;
							CAN0_Green_LED_Set();
							Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
							CAN0_Message_Tx ( &tx_element );
						}
					}

					// Update ETI
					ND_Write ( ND_ETI_LSB, ND_Read ( ND_ETI_LSB ) + 1 );
					if ( ND_Read ( ND_ETI_LSB ) == 0 )
					{
						ND_Write ( ND_ETI_MSB, ND_Read ( ND_ETI_MSB ) + 1 );
					}
					break;

				// Timer event for CAN test data message 0
				case SRT_EVENT_CAN_TEST_DATA0_TIMER:

					can_get_tx_buffer_element_defaults ( &tx_element );
					tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
					tx_element.T1.bit.DLC = 8;
					tx_element.data[0] = CAN_MSG_PROTOCOL_TEST_DATA_RESPONSE;
					tx_element.data[1] = 0;
					tx_element.data[2] = ( uint8_t ) ND_Read ( ND_TEST_DATA_0_TYPE );
					tx_element.data[3] = 0;
					tx_element.data[4] = ( uint8_t ) ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_0_TYPE ) );
					tx_element.data[5] = ( uint8_t ) ( ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_0_TYPE ) ) >> 8 );
					tx_element.data[6] = 0;
					tx_element.data[7] = 0;
					CAN0_Green_LED_Set();
					Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
					CAN0_Message_Tx ( &tx_element );
					break;

				//Timer event for CAN test data message 1
				case SRT_EVENT_CAN_TEST_DATA1_TIMER:

					can_get_tx_buffer_element_defaults ( &tx_element );
					tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
					tx_element.T1.bit.DLC = 8;
					tx_element.data[0] = CAN_MSG_PROTOCOL_TEST_DATA_RESPONSE;
					tx_element.data[1] = 0;
					tx_element.data[2] = ( uint8_t ) ND_Read ( ND_TEST_DATA_1_TYPE );
					tx_element.data[3] = 0;
					tx_element.data[4] = ( uint8_t ) ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_1_TYPE ) );
					tx_element.data[5] = ( uint8_t ) ( ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_1_TYPE ) ) >> 8 );
					tx_element.data[6] = 1;
					tx_element.data[7] = 0;
					CAN0_Green_LED_Set();
					Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
					CAN0_Message_Tx ( &tx_element );
					break;

				// Timer event for CAN test data message 2
				case SRT_EVENT_CAN_TEST_DATA2_TIMER:

					can_get_tx_buffer_element_defaults ( &tx_element );
					tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
					tx_element.T1.bit.DLC = 8;
					tx_element.data[0] = CAN_MSG_PROTOCOL_TEST_DATA_RESPONSE;
					tx_element.data[1] = 0;
					tx_element.data[2] = ( uint8_t ) ND_Read ( ND_TEST_DATA_2_TYPE );
					tx_element.data[3] = 0;
					tx_element.data[4] = ( uint8_t ) ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_2_TYPE ) );
					tx_element.data[5] = ( uint8_t ) ( ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_2_TYPE ) ) >> 8 );
					tx_element.data[6] = 2;
					tx_element.data[7] = 0;
					CAN0_Green_LED_Set();
					Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
					CAN0_Message_Tx ( &tx_element );
					break;

				// Timer event for CAN test data message 3
				case SRT_EVENT_CAN_TEST_DATA3_TIMER:

					can_get_tx_buffer_element_defaults ( &tx_element );
					tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
					tx_element.T1.bit.DLC = 8;
					tx_element.data[0] = CAN_MSG_PROTOCOL_TEST_DATA_RESPONSE;
					tx_element.data[1] = 0;
					tx_element.data[2] = ( uint8_t ) ND_Read ( ND_TEST_DATA_3_TYPE );
					tx_element.data[3] = 0;
					tx_element.data[4] = ( uint8_t ) ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_3_TYPE ) );
					tx_element.data[5] = ( uint8_t ) ( ND_Read ( ( t_node_data ) ND_Read ( ND_TEST_DATA_3_TYPE ) ) >> 8 );
					tx_element.data[6] = 3;
					tx_element.data[7] = 0;
					CAN0_Green_LED_Set();
					Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
					CAN0_Message_Tx ( &tx_element );
					break;

				// Timer event for CAN Tx LED
				case SRT_EVENT_CAN_TX_LED_TIMER:

					CAN0_Green_LED_Clear();
					break;

				// Timer event for CAN Rx LED
				case SRT_EVENT_CAN_RX_LED_TIMER:

					CAN0_Yellow_LED_Clear();
					break;

				// Timer event to update temperature measurement
				case SRT_EVENT_TEMPERATURE_TIMER:

					ND_Write ( ND_TEMP, ND_Read ( ND_TEMP )
					           -	( ND_Read ( ND_TEMP ) >> 4 )
					           + ( Temp_Read() >> 4 ) );
					if ( ND_Read ( ND_TEMP_MIN ) > ND_Read ( ND_TEMP ) )
					{
						ND_Write ( ND_TEMP_MIN, ND_Read ( ND_TEMP ) );
					}
					if ( ND_Read ( ND_TEMP_MAX ) < ND_Read ( ND_TEMP ) )
					{
						ND_Write ( ND_TEMP_MAX,  ND_Read ( ND_TEMP ) );
					}
					break;

				// SRT CAN Message Event
				case SRT_EVENT_CAN0_MESSAGE:

					// Empty CAN Buffer
					do
					{
						// Retrieve CAN Message
						CAN0_Message_Rx ( &rx_element_fifo_0 );
						can_rx_message = ( uint16_t ) ( ( ( rx_element_fifo_0.R0.bit.ID >> 1 ) & CAN_MSG_MASK ) >> CAN_MESSAGE_POS );
						can_rx_node = ( uint8_t ) ( rx_element_fifo_0.R0.bit.ID >> 1 ) & CAN_ID_MASK;

						// CAN_MSG_EXCEPTION message from any node
						if ( can_rx_message == CAN_MSG_EXCEPTION )
						{
							if ( rx_element_fifo_0.R1.bit.DLC == 0 )
							{
								// Shutdown message
							}
							else
							{
								// Exception message
							}
						}

						// CAN_MSG_PROTOCOL message from System Controller
						// (other nodes filtered out)
						if ( can_rx_message == CAN_MSG_PROTOCOL )
						{
							// CAN_MSG_PROTOCOL message from System Controller
							// (other nodes filtered out)

							if ( rx_element_fifo_0.R1.bit.DLC == 0 )
							{
								// Heartbeat from System Controller
							}
							else
							{
								// Protocol message from System Controller
								switch ( rx_element_fifo_0.data[0] )
								{
									case CAN_MSG_PROTOCOL_BROKEN_HEARTBEAT:
										// If CAN Master is broadcasting shutdown exception, follow suit
										if ( rx_element_fifo_0.data[1] == EXC_SHUTDOWN )
										{
											Exception_Handler ( EXC_SHUTDOWN, EXC_TYPE_CRITICAL );
										}
										break;

									case CAN_MSG_PROTOCOL_ROLL_CALL_RESPONSE:
										// Response not required by nodes
										break;

									case CAN_MSG_PROTOCOL_TEST_DATA_RESPONSE:
										// Response not required by nodes
										break;

									case CAN_MSG_PROTOCOL_BUILD_STD_RESPONSE:
										// Response not required by nodes
										break;

									case CAN_MSG_PROTOCOL_ROLL_CALL_REQUEST:
										can_get_tx_buffer_element_defaults ( &tx_element );
										tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
										tx_element.T1.bit.DLC = 6;
										tx_element.data[0] = CAN_MSG_PROTOCOL_ROLL_CALL_RESPONSE;
										tx_element.data[1] = 0;
										tx_element.data[2] = ( uint8_t ) CAN_NODE_SW_ID;
										tx_element.data[3] = CAN_NODE_SW_ID >> 8;
										tx_element.data[4] = ( uint8_t ) ND_Read ( ND_SERIAL_NUMBER );
										tx_element.data[5] = ND_Read ( ND_SERIAL_NUMBER ) >> 8;
										CAN0_Green_LED_Set();
										Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
										CAN0_Message_Tx ( &tx_element );
										break;

									case CAN_MSG_PROTOCOL_HEARTBEAT_REQUEST:
										if ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										{
											if ( rx_element_fifo_0.data[2] == 1 )
											{
												ND_Write ( ND_HEARTBEAT_STATUS, 1 );
											}
											else
											{
												ND_Write ( ND_HEARTBEAT_STATUS, 0 );
											}
										}
										break;

									case CAN_MSG_PROTOCOL_NODE_ID_ASSIGN:

										// Do not enable Node ID to be assigned until Serial Number has
										// been assigned
										if ( ND_Read ( ND_SERIAL_NUMBER ) == 0 )
										{
											break;
										}

										// If SW ID and Serial Number match assign Node ID
										if ( ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										      & ( rx_element_fifo_0.data[2] == ( uint8_t ) CAN_NODE_SW_ID )
										      & ( rx_element_fifo_0.data[3] == ( CAN_NODE_SW_ID >> 8 ) )
										      & ( rx_element_fifo_0.data[4] == ( uint8_t ) ND_Read ( ND_SERIAL_NUMBER ) )
										      & ( rx_element_fifo_0.data[5] == ( ND_Read ( ND_SERIAL_NUMBER ) >> 8 ) ) )
										{
											ND_Write ( ND_CAN_NODE_ID, rx_element_fifo_0.data[6] );
											NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_CAN_NODE_ID ), ND_Read ( ND_CAN_NODE_ID ) );
										}
										break;

									case CAN_MSG_PROTOCOL_TEST_DATA_REQUEST:
										if ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										{
											switch ( rx_element_fifo_0.data[6] )
											{
												case 0:
													ND_Write ( ND_TEST_DATA_0_TYPE, rx_element_fifo_0.data[2] );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_0_TYPE ), ND_Read ( ND_TEST_DATA_0_TYPE ) );
													ND_Write ( ND_TEST_DATA_0_INTERVAL, ( rx_element_fifo_0.data[5] << 8 ) | rx_element_fifo_0.data[4] );
													Timer_Set ( SRT_EVENT_CAN_TEST_DATA0_TIMER, ND_Read ( ND_TEST_DATA_0_INTERVAL ), true );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_0_INTERVAL ), ND_Read ( ND_TEST_DATA_0_INTERVAL ) );
													break;

												case 1:
													ND_Write ( ND_TEST_DATA_1_TYPE, rx_element_fifo_0.data[2] );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_1_TYPE ), ND_Read ( ND_TEST_DATA_1_TYPE ) );
													ND_Write ( ND_TEST_DATA_1_INTERVAL, ( rx_element_fifo_0.data[5] << 8 ) | rx_element_fifo_0.data[4] );
													Timer_Set ( SRT_EVENT_CAN_TEST_DATA1_TIMER, ND_Read ( ND_TEST_DATA_1_INTERVAL ), true );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_1_INTERVAL ), ND_Read ( ND_TEST_DATA_1_INTERVAL ) );
													break;

												case 2:
													ND_Write ( ND_TEST_DATA_2_TYPE, rx_element_fifo_0.data[2] );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_2_TYPE ), ND_Read ( ND_TEST_DATA_2_TYPE ) );
													ND_Write ( ND_TEST_DATA_2_INTERVAL, ( rx_element_fifo_0.data[5] << 8 ) | rx_element_fifo_0.data[4] );
													Timer_Set ( SRT_EVENT_CAN_TEST_DATA2_TIMER, ND_Read ( ND_TEST_DATA_2_INTERVAL ), true );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_2_INTERVAL ), ND_Read ( ND_TEST_DATA_2_INTERVAL ) );
													break;

												case 3:
													ND_Write ( ND_TEST_DATA_3_TYPE, rx_element_fifo_0.data[2] );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_3_TYPE ), ND_Read ( ND_TEST_DATA_3_TYPE ) );
													ND_Write ( ND_TEST_DATA_3_INTERVAL, ( rx_element_fifo_0.data[5] << 8 ) | rx_element_fifo_0.data[4] );
													Timer_Set ( SRT_EVENT_CAN_TEST_DATA3_TIMER, ND_Read ( ND_TEST_DATA_3_INTERVAL ), true );
													NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_TEST_DATA_3_INTERVAL ), ND_Read ( ND_TEST_DATA_3_INTERVAL ) );
													break;
											}
										}
										break;

									case CAN_MSG_PROTOCOL_MIN_MAX_RESET:
										if ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										{
											// Reset max CAN FIFO queue
											ND_Write ( ND_CAN_RXFIFO_MAX, 0 );

											// Reset max/min temperature measurements
											ND_Write ( ND_TEMP_MIN, ND_Read ( ND_TEMP ) );
											ND_Write ( ND_TEMP_MAX, ND_Read ( ND_TEMP ) );
										}
										break;

									case CAN_MSG_PROTOCOL_SERIAL_NO_ASSIGN:
										if ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										{
											ND_Write ( ND_SERIAL_NUMBER, rx_element_fifo_0.data[2] | ( rx_element_fifo_0.data[3] << 8 ) );
											NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_SERIAL_NUMBER ), ND_Read ( ND_SERIAL_NUMBER ) );
											ND_Write ( ND_HW_ISSUE, rx_element_fifo_0.data[4] | ( rx_element_fifo_0.data[5] << 8 ) );
											NVM_Write_uInt ( NVM_NODE_DATA + ( 2 * ND_HW_ISSUE ), ND_Read ( ND_HW_ISSUE ) );
										};
										break;

									case CAN_MSG_PROTOCOL_BUILD_STD_REQUEST:
										if ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										{
											can_get_tx_buffer_element_defaults ( &tx_element );
											tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID ( ( CAN_MSG_PROTOCOL << CAN_MESSAGE_POS ) | ND_Read ( ND_CAN_NODE_ID ) );
											tx_element.T1.bit.DLC = 8;
											tx_element.data[0] = CAN_MSG_PROTOCOL_BUILD_STD_RESPONSE;
											tx_element.data[1] = ND_Read ( ND_CAN_NODE_ID );
											tx_element.data[2] = ( uint8_t ) CAN_NODE_SW_ID;
											tx_element.data[3] = CAN_NODE_SW_ID >> 8;
											tx_element.data[4] = ( uint8_t ) CAN_NODE_SW_ISSUE;
											tx_element.data[5] = CAN_NODE_SW_ISSUE >> 8;
											tx_element.data[6] = ( uint8_t ) ND_Read ( ND_HW_ISSUE );
											tx_element.data[7] = ND_Read ( ND_HW_ISSUE ) >> 8;
											CAN0_Green_LED_Set();
											Timer_Set ( SRT_EVENT_CAN_TX_LED_TIMER, 10, false );
											CAN0_Message_Tx ( &tx_element );
										}
										break;

									case CAN_MSG_PROTOCOL_EXCEPTION_RESET_REQUEST:
										if ( rx_element_fifo_0.data[1] == ND_Read ( ND_CAN_NODE_ID ) )
										{
											Exception_Handler ( EXC_NO_EXCEPTION, EXC_TYPE_NON_CRITICAL );
										}
										break;

									default:
										Exception_Handler ( EXC_UNEXPECTED_CAN_PROTOCOL, EXC_TYPE_NON_CRITICAL );
										break;
								}
							}
						}

					} // Empty CAN Buffer
					while ( CAN0_FIFO_Length() != 0 );

					break; // SRT CAN Message Event

				default:
					Exception_Handler ( EXC_UNEXPECTED_STR_EVENT, EXC_TYPE_CRITICAL );
					break;
			}

		} // SRT Event Queue

	} // SRT Loop

} // SRT_Task_Manager


/////////////////////////////////////////////////////////////////////////////
///
/// Timer_Set - Function that configures and starts a timer event
///
/// @param [in] timer_id - Identifier for timer event to be configured, defined
/// by SRT_Events.h
/// @param [in] period - Period for timer in milliseconds
/// @param [in] repeat - Specification if timer should repeat when completed
/// (true yes)
///
/////////////////////////////////////////////////////////////////////////////

void Timer_Set ( uint8_t timer_id, uint16_t period, uint8_t repeat )
{
	g_timer_events[ timer_id ].timer_count = period;
	g_timer_events[ timer_id ].timer_period = period;
	g_timer_events[ timer_id ].timer_repeat = repeat;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Timer_Update - Function that updates status of timers and raises timer
/// events.  This function needs to be called each millisecond.
///
/////////////////////////////////////////////////////////////////////////////

void Timer_Update ( void )
{
	uint8_t index; /// Loop counter for working through timers

	// Loop through timer events, decrementing timer and creating events at end of timer
	for ( index = SRT_EVENT_NULL + 1; index < SRT_EVENT_TIMER_END; index = index + 1 )
	{
		// Determine if counter is at end of period
		if ( g_timer_events[ index ].timer_count == 1 )
		{
			// Raise timer event
			g_srt_event_queue.Push ( ( t_srt_events ) index );

			if ( g_timer_events[ index ].timer_repeat )
			{
				// Reset repeating timer
				g_timer_events[ index ].timer_count = g_timer_events[ index ].timer_period;
			}
			else
			{
				g_timer_events[ index ].timer_count = 0;
			}
		}
		else
		{
			// Decrement active timers
			if ( g_timer_events[ index ].timer_count )
			{
				g_timer_events[ index ].timer_count = g_timer_events[ index ].timer_count - 1;
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
///
/// Interrupt Handler Routines
///
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
///
/// TC0_Handler - Function containing Hard Real Time (HRT) functions called
/// by Timer 0 Interrupts
///
/// @module TC0 : Response to interrupt
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void TC0_Handler ( void )
{
	// Variable used to sub-divide HRT interrupts into 1mS periods
	static uint16_t hrt_divider = 0;

	/// + Set Synchronous Event Timer pin if this function is being monitored
	#ifdef SYNC_TC0_HANDLER
	Sync_Event_Pin_Set();
	#endif

	/// + TC0_INTERRUPT_FREQ_KHZ Section
	// Increment HRT divider
	hrt_divider = hrt_divider + 1;

	/// + 1mS Section
	if ( hrt_divider == TC0_INTERRUPT_FREQ_KHZ )
	{
		// Reset counter
		hrt_divider = 0;

		// Update timers and raise events
		Timer_Update ();

	} // 1mS Section

	/// + Clear HRT timer interrupt flag
	TC0_Clear();

	/// + Clear Synchronous Event Timer pin if this function is being
	/// monitored
	#ifdef SYNC_TC0_HANDLER
	Sync_Event_Pin_Clear();
	#endif

} // TC0_Handler


/////////////////////////////////////////////////////////////////////////////
///
/// CAN0_Handler - CAN Interrupt Task Manager
///
/// @module CAN0 : Response to interrupt and access to interrupt flags
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void CAN0_Handler ( void )
{
	/// Variable used to hold interrupt flags
	uint32_t can_interrupt_Flags;

	/// + Set Asynchronous Event Timer pin if this function is monitored
	#ifdef ASYNC_CAN0_HANDLER
	Async_Event_Pin_Set();
	#endif

	/// + Illuminate Tx LED if enabled
	#ifdef CAN_LED_ENABLE
	CAN0_Yellow_LED_Set();
	Timer_Set ( SRT_EVENT_CAN_RX_LED_TIMER, 10, false );
	#endif

	// Update CAN FIFO queue monitor
	if ( CAN0_FIFO_Length() > ND_Read ( ND_CAN_RXFIFO_MAX ) )
	{
		ND_Write ( ND_CAN_RXFIFO_MAX, CAN0_FIFO_Length() );

		// If CAN 0 receive FIFO buffer size reached, raise exception
		if ( ND_Read ( ND_CAN_RXFIFO_MAX ) == CONF_CAN0_RX_FIFO_0_NUM )
		{
			Exception_Handler ( EXC_CAN_RX_FIFO_FULL, EXC_TYPE_NON_CRITICAL );
		}
	}

	///+ Read and manage CAN interrupts
	can_interrupt_Flags = REG_CAN0_IR;

	// New Rx FIFO0 CAN message interrupt
	if ( can_interrupt_Flags & CAN_IR_RF0N )
	{
		// Reset flag
		REG_CAN0_IR = CAN_IR_RF0N;

		/// + Send event to SRT Task Manager
		g_srt_event_queue.Push ( SRT_EVENT_CAN0_MESSAGE );
	}

	// CAN message error in arbitration phase interrupt
	if ( can_interrupt_Flags & CAN_IR_PEA )
	{
		// Reset flag - Usually flagged when connection is lost
		REG_CAN0_IR = CAN_IR_PEA;
	}

	// CAN message error in data phase interrupt
	if ( can_interrupt_Flags & CAN_IR_PED )
	{
		// Reset flag
		REG_CAN0_IR = CAN_IR_PED;
		Exception_Handler ( EXC_CAN_ERROR_INTERRUPT, EXC_TYPE_NON_CRITICAL );
	}

	// CAN message RAM access error
	if ( can_interrupt_Flags & CAN_IR_MRAF )
	{
		// Reset flag
		REG_CAN0_IR = CAN_IR_MRAF;
		Exception_Handler ( EXC_CAN_ERROR_INTERRUPT, EXC_TYPE_NON_CRITICAL );
	}

	/// + Clear Asynchronous Event Timer pin if this function is monitored
	#ifdef ASYNC_CAN0_HANDLER
	Async_Event_Pin_Clear();
	#endif

} // CAN0_Handler


/////////////////////////////////////////////////////////////////////////////
///
/// SYSTEM_Handler - System interrupt Task Manager
///
/// @module NVM : Writing of brownout data
/// @module PORT : Reset all pins as inputs
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void SYSTEM_Handler ( void )
{
	/// + Set Asynchronous Event Timer pin if this function is monitored
	#ifdef ASYNC_SYSTEM_HANDLER
	Async_Event_Pin_Set();
	#endif

	/// + Remove all pin loads
	CAN0_Red_LED_Clear();
	CAN0_Green_LED_Clear();
	CAN0_Yellow_LED_Clear();

	/// + Store current value of ETI and recorded temperature range
	NVM_Brownout ( ( ND_Read ( ND_ETI_MSB ) << 16 )
	               | ND_Read ( ND_ETI_LSB ),
	               ( ND_Read ( ND_TEMP_MIN ) << 16 )
	               | ND_Read ( ND_TEMP_MAX ) );

	/// + Indicate that storage is complete and wait for power down
	while ( true )
	{
		#ifdef ASYNC_SYSTEM_HANDLER
		Async_Event_Pin_Set();
		#endif
	}

} // SYSTEM_Handler