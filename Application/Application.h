/////////////////////////////////////////////////////////////////////////////
///
/// @file Application.h
///
/// @brief File containing the application specific macros and headers for
/// the application.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Uppercase URL UID created by www.guidgenerator.com
///@todo Update UID
#ifndef APPLICATION_H_606A29EFEB3946AD94F50274209825CB
#define APPLICATION_H_606A29EFEB3946AD94F50274209825CB


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"


/////////////////////////////////////////////////////////////////////////////
//
// APPLICATION CONFIGURATION DATA
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// Pin allocations
//
/////////////////////////////////////////////////////////////////////////////

#define SYNC_EVENT_PIN_MASK PORT_PA20 ///< Define mask for pin used for synchronous interrupt timing - Test 1 - TP13
#define ASYNC_EVENT_PIN_MASK PORT_PA21 ///< Define mask for pin used for asynchronous interrupt timing - Test 2 TP14
#define CAN0_RED_LED_PIN_MASK PORT_PA22 ///< Define mask for pin used for driving the CAN0 red LED
#define SRT_LOOP_PIN_MASK PORT_PA23 ///< Define mask for pin used for SRT loop timing - Test 3 TP12
#define FUNCTION_TIMER_PIN_MASK PORT_PA22 ///< Define mask for pin used for timing functions - Test 3 TP12
#define CAN0_TX_MASK PORT_PA24G_CAN0_TX ///< Define mask for pin used for driving the CAN Tx
#define CAN0_RX_MASK PORT_PA25G_CAN0_RX ///< Define mask for pin used for driving the CAN Rx
#define CAN0_GREEN_LED_PIN_MASK PORT_PB17 ///< Define mask for pin used for driving the CAN0 green LED
#define CAN0_YELLOW_LED_PIN_MASK PORT_PA27 ///< Define mask for pin used for driving the CAN0 yellow LED


/////////////////////////////////////////////////////////////////////////////
//
// Test pin enablers
//
/////////////////////////////////////////////////////////////////////////////

#define SRT_LOOP ///< Define that enables timing of the SRT Loop Timing Pin
#define SYNC_TC0_HANDLER ///< Define that enables toggling of the Synchronous Test Pin for the TC0_Handler
#define ASYNC_EIC_HANDLER ///< Define that enables toggling of Asynch Timing Pin for EIC_Handler
//#define ASYNC_CAN0_HANDLER ///< Define that enables toggling of Asynch Timing Pin for CAN0_Handler
//#define ASYNC_SYSTEM_HANDLER ///< Define that enables toggling of Asynch Timing Pin and SRT Loop Pin for SYSTEM_Handler
//#define ASYNC_ADC0_HANDLER ///< Define that enables toggling of Asynch Timing Pin for ADC0_Handler
//#define CAN0_Message_Tx_FUNCTION ///< Define that enables toggling of Function Timing Pin for CAN_Message_Tx function


/////////////////////////////////////////////////////////////////////////////
//
// HRT Interrupt Frequency Configuration
//
/////////////////////////////////////////////////////////////////////////////

#define TC0_INTERRUPT_FREQ_KHZ 1 ///< Define TC0 interrupt frequency in kHz


/////////////////////////////////////////////////////////////////////////////
//
// CAN Test Node Software ID and Issue
//
/////////////////////////////////////////////////////////////////////////////

/// Place holder for Node Type (SW ID)
#define CAN_NODE_SW_ID 0xFFFF

/// Place holder for Node Type Issue
#define CAN_NODE_SW_ISSUE 0xFFFF

/////////////////////////////////////////////////////////////////////////////
//
// Node Data Parameters
//
/////////////////////////////////////////////////////////////////////////////

/// List of all the Node Data parameters that are
/// shared internally within this application and are made available on
/// request over the CAN Bus via the TEST_DATA_REQUEST protocol.

typedef enum
{
	ND_INITIALISED, ///< Flag to indicate if node has been initiated before
	ND_SERIAL_NUMBER, ///< Unique serial number for node
	ND_CAN_NODE_ID, ///< CAN Node ID assigned to node
	ND_HW_ISSUE, ///< CAN Node Hardware Issue
	ND_HEARTBEAT_STATUS, ///< Indication on if heartbeat is enabled 0:Disabled; 1:Enabled
	ND_EXCEPTION_STATUS, ///< Current exception status of node.  See Node_Exceptions.h
	ND_ETI_LSB, ///< Elapsed Time Indication of powered operation of node in seconds (LSB)
	ND_ETI_MSB, ///< Elapsed Time Indication of powered operation of node in seconds (MSB)
	ND_TEMP_MIN, ///< Minimum temperature measured by node in degrees C x100
	ND_TEMP, ///< Minimum temperature measured by node in degrees C x100
	ND_TEMP_MAX, ///< Maximum temperature measured by node in degrees C x100
	ND_CAN_RXFIFO_MAX, ///< Maximum CAN Rx FIFO Queue length
	ND_TEST_DATA_0_TYPE, ///< Data type for Test Data 0
	ND_TEST_DATA_0_INTERVAL, ///< Transmit interval in mS for Test Data 0
	ND_TEST_DATA_1_TYPE, ///< Data type for Test Data 1
	ND_TEST_DATA_1_INTERVAL, ///< Transmit interval in mS for Test Data 1
	ND_TEST_DATA_2_TYPE, ///< Data type for Test Data 2
	ND_TEST_DATA_2_INTERVAL, ///< Transmit interval in mS for Test Data 2
	ND_TEST_DATA_3_TYPE, ///< Data type for Test Data 3
	ND_TEST_DATA_3_INTERVAL, ///< Transmit interval in mS for Test Data 3
	ND_SIZE ///< Dummy to enable size of array to be initiated

} t_node_data;












/////////////////////////////////////////////////////////////////////////////
//
// SRT Events
//
/////////////////////////////////////////////////////////////////////////////

/// Define length of SRT event FIFO buffer
#define SRT_EVENT_FIFO_LENGTH 20


/// List of all the possible SRT event buffer events, starting with null, then
/// synchronous events raised by the timer library in priority order, followed
/// by asynchronous events raised by interrupts

typedef enum
{
	SRT_EVENT_NULL, ///< SRT non-event
	SRT_EVENT_1HZ_TIMER, ///< Timer event for one second tick
	SRT_EVENT_CAN_TEST_DATA0_TIMER, ///< Timer event for CAN test data message 0
	SRT_EVENT_CAN_TEST_DATA1_TIMER, ///< Timer event for CAN test data message 1
	SRT_EVENT_CAN_TEST_DATA2_TIMER, ///< Timer event for CAN test data message 2
	SRT_EVENT_CAN_TEST_DATA3_TIMER, ///< Timer event for CAN test data message 3
	SRT_EVENT_CAN_TX_LED_TIMER, ///< Timer event for CAN Tx LED
	SRT_EVENT_CAN_RX_LED_TIMER, ///< Timer event for CAN Rx LED
	SRT_EVENT_TEMPERATURE_TIMER, ///< Timer event to update temperature measurement
	SRT_EVENT_TIMER_END, ///< Marker of end of events for timer
	SRT_EVENT_CAN0_MESSAGE, ///< CAN message received event
	SRT_EVENT_SIZE ///< Dummy to enable size of array to be initiated

} t_srt_events;


/////////////////////////////////////////////////////////////////////////////
//
// Node Exception Conditions
//
/////////////////////////////////////////////////////////////////////////////

/// List of all the Node Exceptions that are
/// shared internally within this application and are made available on
/// request over the CAN Bus via the BROKEN_HEARTBEAT protocol.

typedef enum
{
	// Exceptions which are common to all nodes
	EXC_NO_EXCEPTION, ///< No exception
	EXC_SHUTDOWN, ///< Re-broadcast of system shutdown request

	// Application specific exceptions in order of severity
	EXC_WDT_RESET, ///< Reset caused by Watchdog Timer
	EXC_SRT_FIFO_EMPTY, ///< SRT event FIFO pulled when empty
	EXC_SRT_FIFO_FULL, ///< SRT event FIFO push when full
	EXC_CAN_RX_FIFO_FULL, ///< CAN receive FIFO at reached limit
	EXC_CAN_ERROR_INTERRUPT, ///< CAN Interrupt raised for fault condition
	EXC_UNEXPECTED_CAN_PROTOCOL, ///< CAN Protocol message with no handler
	EXC_UNEXPECTED_STR_EVENT, ///< SRT event raised with no handler

} t_node_exceptions;


/////////////////////////////////////////////////////////////////////////////
//
// NVM Configurations
//
/////////////////////////////////////////////////////////////////////////////

#define NVM_NODE_DATA 0x00400000 ///< Memory location address within NVM for start of global variables storage area
#define NVM_BROWNOUT_PAGE 0x00400100 ///< Memory location address within NVM for storing brownout page


/////////////////////////////////////////////////////////////////////////////
//
// Brown Out Voltage
//
/////////////////////////////////////////////////////////////////////////////

#define BROWN_OUT_VOLTAGE 0x9 ///< 	SUPC : REG_SUPC_BODVDD brownout detection at 2.92-3.00v (this may needs confirming with programmer)



/////////////////////////////////////////////////////////////////////////////
//
// Function prototypes
//
/////////////////////////////////////////////////////////////////////////////

void Initialize ( void );
void SRT_Task_Manager ( void );
void Timer_Set ( uint8_t, uint16_t, uint8_t );
void Timer_Update ( void );


#endif // APPLICATION_H_606A29EFEB3946AD94F50274209825CB