/////////////////////////////////////////////////////////////////////////////
///
/// @file SUPC_Drivers.cpp
///
/// @brief File containing functions and definitions for using the SUPC
/// Module, which are specific to this range of micro controllers. The
/// brownout needs to be programmed manually via programmer.
///
/// @copyright Confidential.  Copyright (C) 2020, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"
#include "Application.h"



/////////////////////////////////////////////////////////////////////////////
///
/// SUPC_Init - Function that initializes the SUPC
///
/////////////////////////////////////////////////////////////////////////////

void SUPC_Init ( void )
{
	/// @module SUPC : Configure brownout detection at 2.92-3.00v
	/// (this may needs confirming with programmer)
	REG_SUPC_BODVDD = SUPC_BODVDD_LEVEL ( BROWN_OUT_VOLTAGE )
	                  | SUPC_BODVDD_ACTION_INT
	                  | SUPC_BODVDD_HYST
	                  | SUPC_BODVDD_ENABLE;

	/// @module SUPC : Enable SUPC Interrupts
	REG_SUPC_INTFLAG = 0xFFFF;
	REG_SUPC_INTENSET = SUPC_INTENSET_BODVDDDET;
	NVIC_EnableIRQ ( SYSTEM_IRQn );
}