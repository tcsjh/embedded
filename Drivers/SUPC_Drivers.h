/////////////////////////////////////////////////////////////////////////////
///
/// @file SUPC_Drivers.h
///
/// @brief File containing headers and inline functions for using the SUPC
/// Module, which are specific to this range of micro controllers.
///
/// @copyright Confidential.  Copyright (C) 2020, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Function prototypes

void SUPC_Init ( void );