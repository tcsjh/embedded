/////////////////////////////////////////////////////////////////////////////
///
/// @file CLK_Drivers.cpp
///
/// @brief File containing functions for the micro-controller clocks which
/// are configured by the MCLK, OSC48CLK, XOSC32KCTRL, DPLLCLK and GCLK
/// modules.  These functions provide a 48MHz main clock using the external
/// 32KHz crystal and the DPLL and 32kHz/1Hz directly from the
/// external 32kHz crystal processed by the XOSC32KCTRL module.  These
/// functions  are specific to this micro-controller family.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"


/////////////////////////////////////////////////////////////////////////////
///
/// Clocks_Init - Function that initializes the micro-controller clocks.
///
/// The micro-controller Generic Clocks are configured as follows::
///
/// | GCLK |  CLK   | FREQ  |
/// | :--: | :----: | :---: |
/// | 0    | DPLL   | 48MHz |
/// | 1    | ExtOsc | 32KHz	|
/// | 2    |        |       |
/// | 3    |        |       |
/// | 4    |        |       |
/// | 5    |        |       |
/// | 6    |        |       |
/// | 7    |        |       |
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void Clocks_Init ( void )
{
	// Set flash wait state
	REG_NVMCTRL_CTRLB = NVMCTRL_CTRLB_RWS ( 3 );

	/// + @module MCLK : Enable Master Clock to OSC32K Controller
	REG_MCLK_APBAMASK = REG_MCLK_APBAMASK | MCLK_APBAMASK_OSC32KCTRL;

	/// + @module XOSC32KCTRL : Enable external 32kHz clock and provide 1kHz
	/// reference
	REG_OSC32KCTRL_XOSC32K = 0x0000					// Set to 0 then or with required modes
	                         | OSC32KCTRL_XOSC32K_STARTUP ( 0x4 )	// 500mS startup
	                         | OSC32KCTRL_XOSC32K_XTALEN			// External crystal enabled
	                         | OSC32KCTRL_XOSC32K_EN1K			// 1KHz output for RTC
	                         | OSC32KCTRL_XOSC32K_EN32K			// 32KHz output enabled
	                         | OSC32KCTRL_XOSC32K_RUNSTDBY		// Run in Standby
	                         | OSC32KCTRL_XOSC32K_ENABLE;		// Module enabled

	/// + @module XOSC32KCTRL : Wait for clock to start and synchronise
	while ( ! ( REG_OSC32KCTRL_STATUS & OSC32KCTRL_STATUS_XOSC32KRDY ) );

	/// + @module GCLK : Ensure it has re-initialized correctly
	REG_GCLK_CTRLA = GCLK_CTRLA_SWRST;
	while ( REG_GCLK_CTRLA & GCLK_CTRLA_SWRST );

	/// + @module GCLK : Set GCLK1 to use external 32KHz oscillator
	REG_GCLK_GENCTRL1 = 0x00000000UL				// Set to 0 then or with required modes
	                    | GCLK_GENCTRL_DIV ( 1 )				// No division
	                    | GCLK_GENCTRL_RUNSTDBY				// Run in Standby
	                    | GCLK_GENCTRL_SRC_XOSC32K			// Source external 32KHz crystal
	                    | GCLK_GENCTRL_GENEN;				// Enable

	/// + @module GCLK : Wait for GCLK to sync
	while ( REG_GCLK_SYNCBUSY & GCLK_SYNCBUSY_GENCTRL1 );

	/// @module DPLLCLK : DPLL runs in standby
	REG_OSCCTRL_DPLLCTRLA = OSCCTRL_DPLLCTRLA_RUNSTDBY;

	/// @module DPLLCLK : Set ratio for 48MHz
	REG_OSCCTRL_DPLLRATIO = 0x00000000UL
	                        | OSCCTRL_DPLLRATIO_LDRFRAC ( 84 )		//
	                        | OSCCTRL_DPLLRATIO_LDR ( 1463 );		// 32.768KHz * (1 + 1463.84) = 48MHz

	/// @module DPLLCLK : Wait for sync
	while ( REG_OSCCTRL_DPLLSYNCBUSY & OSCCTRL_DPLLSYNCBUSY_DPLLRATIO );

	/// @module DPLLCLK : Set source
	REG_OSCCTRL_DPLLCTRLB = 0x00000000UL			// Set to 0 then or with required modes
	                        | OSCCTRL_DPLLCTRLB_DIV ( 0 )			// No input division
	                        | OSCCTRL_DPLLCTRLB_LTIME ( 0 )		// Automatic lock time
	                        | OSCCTRL_DPLLCTRLB_REFCLK ( 0 )		// XOSC32K as source
	                        | OSCCTRL_DPLLCTRLB_FILTER ( 0 );		// Default filter

	REG_OSCCTRL_DPLLPRESC = OSCCTRL_DPLLPRESC_PRESC ( 0 );			// No divide = 48MHz

	/// @module DPLLCLK : Wait for prescaler to sync
	while ( REG_OSCCTRL_DPLLSYNCBUSY & OSCCTRL_DPLLSYNCBUSY_DPLLPRESC );

	/// @module DPLLCLK : Enable DPLL
	REG_OSCCTRL_DPLLCTRLA |= OSCCTRL_DPLLCTRLA_ENABLE;

	/// @module DPLLCLK : Wait for sync
	while ( REG_OSCCTRL_DPLLSYNCBUSY & OSCCTRL_DPLLSYNCBUSY_ENABLE );

	/// @module DPLLCLK : Wait for DPLL to lock
	while ( ! ( REG_OSCCTRL_DPLLSTATUS & OSCCTRL_DPLLSTATUS_CLKRDY ) );
	while ( ! ( REG_OSCCTRL_DPLLSTATUS & OSCCTRL_DPLLSTATUS_LOCK ) );

	/// @module GCLK0 : Configure GCLK0 (CPU) source to DPLL
	REG_GCLK_GENCTRL0 = 0x00000000UL				// Set to 0 then or with required modes
	                    | GCLK_GENCTRL_DIV ( 1 )				// No division
	                    | GCLK_GENCTRL_RUNSTDBY				// Run in Standby
	                    | GCLK_GENCTRL_SRC_DPLL96M			// Source DPLL
	                    | GCLK_GENCTRL_GENEN;				// Enable

	/// @module GCLK0 : Wait for GCLK to sync
	while ( REG_GCLK_SYNCBUSY & GCLK_SYNCBUSY_GENCTRL0 );
}