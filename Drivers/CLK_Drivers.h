/////////////////////////////////////////////////////////////////////////////
///
/// @file CLK_Drivers.h
///
/// @brief File containing headers for the clock modules.  These are
/// specific to this micro-controller family.
///
/// @copyright Confidential.  Copyright (C) 2015-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Defines to share clock frequencies

#define GCLK0_FREQ_KHZ 48000	///< Define GCLK0 frequency in kHz
#define GCLK1_FREQ_KHZ 32		///< Define GCLK1 frequency in kHz


// Function prototypes

void Clocks_Init ( void );