/////////////////////////////////////////////////////////////////////////////
///
/// @file NVM_Drivers.cpp
///
/// @brief File containing functions for the NVM which are specific to
/// this range of micro controllers.
///
/// @copyright Confidential.  Copyright (C) 2020, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"
#include "Application.h"
#include "NVM_Drivers.h"



/////////////////////////////////////////////////////////////////////////////
///
/// NVM_Init - Function that initializes the Non Volatile Memory controller
///
/// @module NVM : Disable manual write to NVM and set read wait states
///
/////////////////////////////////////////////////////////////////////////////

void NVM_Init ( void )
{
	REG_NVMCTRL_CTRLB = NVMCTRL_CTRLB_RWS ( 3 );
}


/////////////////////////////////////////////////////////////////////////////
///
/// NVM_Write_uChar - Function that stores a uChar into NVM
///
/// This routine may take a number of milliseconds to complete, during which
/// interrupts are disabled.  Care should be taken with real time
/// applications.
///
/// @module NVM : Write access to Non Volatile Memory
/// @param [in] Address - Memory address where data is the be stored in
/// bytes
/// @param [in] Data - Data to be stored in Non Volatile Memory
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void NVM_Write_uChar ( uint32_t Address, uint8_t Data )
{
	uint32_t Page_Base; ///< Address of bottom of page to be written
	uint32_t Page[ NVMCTRL_PAGE_SIZE ]; ///< Array to store page of data, 256 bytes
	uint8_t Loop; ///< Loop counter
	uint32_t Mask;  ///< Variable to hold data mask
	volatile unsigned int * Memory_Ptr; ///< Pointer to memory locations

	/// + Set Function Timer pin if this function is being monitored
	#ifdef Timer_NVM_Write_uChar
	Function_Timer_Pin_Set();
	#endif

	/// + Disable_Interrupts
	__disable_irq();

	/// + Calculate start of page
	Page_Base = Address & 0xFFFFFF00;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) Page_Base;

	/// + Store page of data
	for ( Loop = 0; Loop < NVMCTRL_PAGE_SIZE; Loop = Loop + 1 )
	{
		Page[Loop] = *Memory_Ptr;
		Memory_Ptr = Memory_Ptr + 1;
	}

	/// + Modify stored page with new data
	Mask = ~ ( 0xFF << ( ( Address & 0x03 ) * 8 ) );
	Page[ ( 0xFF & Address ) >> 2 ]
	   = ( Page[ ( 0xFF & Address ) >> 2 ] & Mask )
	     | ( Data << ( ( Address & 0x03 ) * 8 ) );

	/// + Erase page
	REG_NVMCTRL_ADDR = ( unsigned int ) ( Page_Base >> 1 );
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEER;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) Page_Base;

	/// + Store page of data
	for ( Loop = 0; Loop < NVMCTRL_PAGE_SIZE; Loop = Loop + 1 )
	{
		while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
		*Memory_Ptr = Page[Loop];
		Memory_Ptr = Memory_Ptr + 1;
	}

	/// + Complete final write as boundary has not been crossed
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEWP;
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );


	/// + Enable_Interrupts
	__enable_irq();

	/// + If this function is being monitored, clear Function Timer pin
	#ifdef Timer_NVM_Write_uChar
	Function_Timer_Pin_Clear();
	#endif
}


/////////////////////////////////////////////////////////////////////////////
///
/// NVM_Write_uInt - Function that stores a uInt into NVM
///
/// This routine may take a number of milliseconds to complete, during which
/// interrupts are disabled.  Care should be taken with real time
/// applications.
///
/// @module NVM : Write access to Non Volatile Memory
/// @param [in] Address - Memory address where data is the be stored in
/// bytes, which must be a multiple of 2
/// @param [in] Data - Data to be stored in Non Volatile Memory
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void NVM_Write_uInt ( uint32_t Address, unsigned int Data )
{
	uint32_t Page_Base; ///< Address of bottom of page to be written
	uint32_t Page[ NVMCTRL_PAGE_SIZE ]; ///< Array to store page of data, 256 bytes
	uint8_t Loop; ///< Loop counter
	volatile unsigned int * Memory_Ptr; ///< Pointer to memory locations

	/// + Set Function Timer pin if this function is being monitored
	#ifdef Timer_NVM_Write_uInt
	Function_Timer_Pin_Set();
	#endif

	/// + Disable_Interrupts
	__disable_irq();

	/// + Calculate start of page
	Page_Base = Address & 0xFFFFFF00;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) Page_Base;

	/// + Store page of data
	for ( Loop = 0; Loop < NVMCTRL_PAGE_SIZE; Loop = Loop + 1 )
	{
		Page[Loop] = *Memory_Ptr;
		Memory_Ptr = Memory_Ptr + 1;
	}

	/// + Modify stored page with new data
	if ( Address & 0x02 )
	{
		Page[ ( 0xFF & Address ) >> 2 ]
		   = ( Page[ ( 0xFF & Address ) >> 2 ] & 0x0000FFFF )
		     | ( Data << 16 );
	}
	else
	{
		Page[ ( 0xFF & Address ) >> 2 ]
		   = ( Page[ ( 0xFF & Address ) >> 2 ] & 0xFFFF0000 ) |  Data;
	}

	/// + Erase page
	REG_NVMCTRL_ADDR = ( unsigned int ) ( Page_Base >> 1 );
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEER;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) Page_Base;

	/// + Store page of data
	for ( Loop = 0; Loop < NVMCTRL_PAGE_SIZE; Loop = Loop + 1 )
	{
		while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
		*Memory_Ptr = Page[Loop];
		Memory_Ptr = Memory_Ptr + 1;
	}

	/// + Complete final write as boundary has not been crossed
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEWP;
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );

	/// + Enable_Interrupts
	__enable_irq();

	/// + If this function is being monitored, clear Function Timer pin
	#ifdef Timer_NVM_Write_uInt
	Function_Timer_Pin_Clear();
	#endif
}


/////////////////////////////////////////////////////////////////////////////
///
/// NVM_Write_uLong - Function that stores a uLong into NVM
///
/// This routine may take a number of milliseconds to complete, during which
/// interrupts are disabled.  Care should be taken with real time
/// applications.
///
/// @module NVM : Write access to Non Volatile Memory
/// @param [in] Address - Memory address where data is the be stored in
/// bytes, which must be a multiple of 4
/// @param [in] Data - Data to be stored in Non Volatile Memory
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void NVM_Write_uLong ( uint32_t Address, uint32_t Data )
{
	uint32_t Page_Base; ///< Address of bottom of page to be written
	uint32_t Page[ NVMCTRL_PAGE_SIZE ]; ///< Array to store page of data, 256 bytes
	uint8_t Loop; ///< Loop counter
	volatile unsigned int * Memory_Ptr; ///< Pointer to memory locations

	/// + Set Function Timer pin if this function is being monitored
	#ifdef Timer_NVM_Write_uLong
	Function_Timer_Pin_Set();
	#endif

	/// + Disable_Interrupts
	__disable_irq();

	/// + Calculate start of page
	Page_Base = Address & 0xFFFFFF00;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) Page_Base;

	/// + Store page of data
	for ( Loop = 0; Loop < NVMCTRL_PAGE_SIZE; Loop = Loop + 1 )
	{
		Page[Loop] = *Memory_Ptr;
		Memory_Ptr = Memory_Ptr + 1;
	}

	/// + Modify stored page with new data
	Page[ ( 0xFF & Address ) >> 2 ] = Data;

	/// + Erase page
	REG_NVMCTRL_ADDR = ( unsigned int ) ( Page_Base >> 1 );
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEER;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) Page_Base;

	/// + Store page of data
	for ( Loop = 0; Loop < NVMCTRL_PAGE_SIZE; Loop = Loop + 1 )
	{
		while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
		*Memory_Ptr = Page[Loop];
		Memory_Ptr = Memory_Ptr + 1;
	}

	/// + Complete final write as boundary has not been crossed
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEWP;
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );

	/// + Enable_Interrupts
	__enable_irq();

	/// + If this function is being monitored, clear Function Timer pin
	#ifdef Timer_NVM_Write_uIlong
	Function_Timer_Pin_Clear();
	#endif
}


/////////////////////////////////////////////////////////////////////////////
///
/// NVM_Brownout - Function that over rights 2 uLong onto the brownout page
///
/// @module NVM : Write access to Non Volatile Memory
/// @param [in] Data1 - Data to be stored in lower Non Volatile Memory
/// @param [in] Data2 - Data to be stored in lower Non Volatile Memory
///
/// Macro NVM_BROWNOUT_PAGE, which defines the address in NVM for a page
/// which will be overwritten by this function, must be defined before this
/// function
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void NVM_Brownout ( uint32_t Data1, uint32_t Data2 )
{
	volatile unsigned int * Memory_Ptr; ///< Pointer to memory locations

	/// + Set Function Timer pin if this function is being monitored
	#ifdef Timer_NVM_Brownout_uLong
	Function_Timer_Pin_Set();
	#endif

	///// + Erase page
	REG_NVMCTRL_ADDR = ( unsigned int ) ( NVM_BROWNOUT_PAGE >> 1 );
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEER;

	/// + Point memory point to location of base of page
	Memory_Ptr = ( unsigned int * ) NVM_BROWNOUT_PAGE;

	/// + Store page of data
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	*Memory_Ptr = Data1;
	Memory_Ptr = Memory_Ptr + 1;
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	*Memory_Ptr = Data2;

	/// + Write to memory
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );
	REG_NVMCTRL_CTRLA = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_RWWEEWP;
	while ( ( REG_NVMCTRL_INTFLAG & NVMCTRL_INTENCLR_READY ) == 0 );

	/// + If this function is being monitored, clear Function Timer pin
	#ifdef Timer_NVM_Brownout_uLong
	Function_Timer_Pin_Clear();
	#endif
}