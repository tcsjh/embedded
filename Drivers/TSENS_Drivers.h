/////////////////////////////////////////////////////////////////////////////
///
/// @file TSENS_Drivers.h
///
/// @brief File containing inline functions for the Temperature Sensing
/// Module which Module, which are specific to this range of micro
/// controllers.
///
/// @copyright Confidential.  Copyright (C) 2015-2020, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Inline function definitions

/////////////////////////////////////////////////////////////////////////////
///
/// Temp_Read - Function that reads the internal temperature sensor
///
/// @return Temperature reading from sensor in degrees C x100
/// @module TSENS : Reads sensor
///
/////////////////////////////////////////////////////////////////////////////

static inline unsigned int Temp_Read ( void )
{
	unsigned int Temperature;  ///< Variable that holds the temperature sensor measurement register value

	// Ensure measurement is complete
	while ( ! ( REG_TSENS_INTFLAG & TSENS_INTFLAG_RESRDY ) );

	// Store temperature
	Temperature = REG_TSENS_VALUE;

	// Initiate next measurement
	REG_TSENS_CTRLB = TSENS_CTRLB_START;

	return Temperature;
}


// Function prototypes

void TSEN_Init ( void );
unsigned int Temp_Read ( void );