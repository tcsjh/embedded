/////////////////////////////////////////////////////////////////////////////
///
/// @file RSTC_Drivers.h
///
/// @brief File containing definitions the RSTC which are specific to this range
/// of micro controllers.
///
/// @copyright Confidential.  Copyright (C) 2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Macros functions

#define WDT_Reset() ( REG_RSTC_RCAUSE & RSTC_RCAUSE_WDT ) ///< WDT_Reset() - Function that returns true if last reset was cause by the