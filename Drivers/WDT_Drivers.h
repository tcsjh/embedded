/////////////////////////////////////////////////////////////////////////////
///
/// @file WDT_Drivers.h
///
/// @brief File containing headers and inline functions for using the WDT
/// Module, which are specific to this range of micro controllers.
///
/// @copyright Confidential.  Copyright (C) 2020, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


// Inline function definitions

/////////////////////////////////////////////////////////////////////////////
///
/// WDT_Clear - Function that clears the WDT time-out
///
/// @module WDT : Clearing WDT time-out
///
/////////////////////////////////////////////////////////////////////////////

static inline void WDT_Clear ( void )
{
	// Ensure WDT clear is not busy
	while ( REG_WDT_SYNCBUSY & WDT_SYNCBUSY_CLEAR );

	// Clear WDT
	REG_WDT_CLEAR = WDT_CLEAR_CLEAR_KEY;
}


// Function prototypes

void WDT_Init ( void );