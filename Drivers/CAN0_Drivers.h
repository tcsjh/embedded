/////////////////////////////////////////////////////////////////////////////
///
/// @file CAN0_Drivers.h
///
/// @brief File containing headers for the CAN Module which are
/// specific to this micro-controller family.
///
/// @copyright Confidential.  Copyright (C) 2020-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Application specific CAN settings
//
/////////////////////////////////////////////////////////////////////////////

/// CAN Rx and Tx LED configuration
#define CAN0_LED_ENABLE ///< Define that enables CAN Tx and Rx LED
#define CAN0_LED_PERIOD 10 ///< Define period in mS that the CAN Tx and Rx LED should be illuminated


/*
 * Below is the message RAM setting, it will be stored in the system RAM.
 * Please adjust the message size according to your application.
 */
#define CONF_CAN0_RX_FIFO_0_NUM         16            /* Range: 1..64 */
#define CONF_CAN0_RX_FIFO_1_NUM         16            /* Range: 1..64 */
#define CONF_CAN0_RX_BUFFER_NUM         16            /* Range: 1..64 */
#define CONF_CAN0_TX_BUFFER_NUM         4             /* Range: 1..16 */
#define CONF_CAN0_TX_FIFO_QUEUE_NUM     4             /* Range: 1..16 */
#define CONF_CAN0_TX_EVENT_FIFO         8             /* Range: 1..32 */

#define CONF_CAN0_RX_STANDARD_ID_FILTER_NUM     32    /* Range: 1..128 */
#define CONF_CAN0_RX_EXTENDED_ID_FILTER_NUM     16    /* Range: 1..64 */

//#define CONF_CAN1_RX_FIFO_0_NUM         16            /* Range: 1..64 */
//#define CONF_CAN1_RX_FIFO_1_NUM         16            /* Range: 1..64 */
//#define CONF_CAN1_RX_BUFFER_NUM         16            /* Range: 1..64 */
//#define CONF_CAN1_TX_BUFFER_NUM         4             /* Range: 1..16 */
//#define CONF_CAN1_TX_FIFO_QUEUE_NUM     4             /* Range: 1..16 */
//#define CONF_CAN1_TX_EVENT_FIFO         8             /* Range: 1..32 */

//#define CONF_CAN1_RX_STANDARD_ID_FILTER_NUM     32    /* Range: 1..128 */
//#define CONF_CAN1_RX_EXTENDED_ID_FILTER_NUM     16    /* Range: 1..64 */

/* The value should be 8/12/16/20/24/32/48/64. */
#define CONF_CAN0_ELEMENT_DATA_SIZE         8

/*
 * The setting of the nominal bit rate is based on the GCLK_CAN is 48M which you can
 * change in the conf_clock.h. Below is the default configuration. The
 * time quanta is 48MHz / (5+1) =  8MHz. And each bit is (1 + NTSEG1 + 1 + NTSEG2 + 1) = 16 time
 * quanta which means the bit rate is 8MHz/16=500KHz.
 */
/* Nominal bit Baud Rate Prescaler */
#define CONF_CAN_NBTP_NBRP_VALUE	5
/* Nominal bit (Re)Synchronization Jump Width */
#define CONF_CAN_NBTP_NSJW_VALUE    3
/* Nominal bit Time segment before sample point */
#define CONF_CAN_NBTP_NTSEG1_VALUE  10
/* Nominal bit Time segment after sample point */
#define CONF_CAN_NBTP_NTSEG2_VALUE  3

/*
 * The setting of the data bit rate is based on the GCLK_CAN is 48M which you can
 * change in the conf_clock.h. Below is the default configuration. The
 * time quanta is 48MHz / (5+1) =  8MHz. And each bit is (1 + DTSEG1 + 1 + DTSEG2 + 1) = 16 time
 * quanta which means the bit rate is 8MHz/16=500KHz.
 */
/* Data bit Baud Rate Prescaler */
#define CONF_CAN_DBTP_DBRP_VALUE    5
/* Data bit (Re)Synchronization Jump Width */
#define CONF_CAN_DBTP_DSJW_VALUE    3
/* Data bit Time segment before sample point */
#define CONF_CAN_DBTP_DTSEG1_VALUE  10
/* Data bit Time segment after sample point */
#define CONF_CAN_DBTP_DTSEG2_VALUE  3


/////////////////////////////////////////////////////////////////////////////
//
// CAN Data Structures
//
/////////////////////////////////////////////////////////////////////////////

/* -------- CAN_RX_ELEMENT_R0 : (CAN RX element: 0x00) (R/W 32) Rx Element R0 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t ID: 29;           /*!< bit:  0..28  Identifier */
		uint32_t RTR: 1;           /*!< bit:  29     Remote Transmission Request */
		uint32_t XTD: 1;           /*!< bit:  30     Extended Identifier */
		uint32_t ESI: 1;           /*!< bit:  31     Error State Indicator */
	} bit;                       /*!< Structure used for bit access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_RX_ELEMENT_R0_Type;

#define CAN_RX_ELEMENT_R0_ID_Pos          0
#define CAN_RX_ELEMENT_R0_ID_Msk          (0x1FFFFFFFul << CAN_RX_ELEMENT_R0_ID_Pos)
#define CAN_RX_ELEMENT_R0_ID(value)       ((CAN_RX_ELEMENT_R0_ID_Msk & ((value) << CAN_RX_ELEMENT_R0_ID_Pos)))
#define CAN_RX_ELEMENT_R0_RTR_Pos         29
#define CAN_RX_ELEMENT_R0_RTR             (0x1ul << CAN_RX_ELEMENT_R0_RTR_Pos)
#define CAN_RX_ELEMENT_R0_XTD_Pos         30
#define CAN_RX_ELEMENT_R0_XTD             (0x1ul << CAN_RX_ELEMENT_R0_XTD_Pos)
#define CAN_RX_ELEMENT_R0_ESI_Pos         31
#define CAN_RX_ELEMENT_R0_ESI             (0x1ul << CAN_RX_ELEMENT_R0_ESI_Pos)

/* -------- CAN_RX_ELEMENT_R1 : (CAN RX element: 0x01) (R/W 32) Rx Element R1 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t RXTS: 16;         /*!< bit: 0..15   Rx Timestamp */
		uint32_t DLC: 4;           /*!< bit: 16..19  Data Length Code */
		uint32_t BRS: 1;           /*!< bit: 20      Bit Rate Switch */
		uint32_t FDF: 1;           /*!< bit: 21      FD Format */
		uint32_t : 2;              /*!< bit: 22..23  Reserved */
		uint32_t FIDX: 7;          /*!< bit: 24..30  Filter Index */
		uint32_t ANMF: 1;          /*!< bit: 31      Accepted Non-matching Frame */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_RX_ELEMENT_R1_Type;

#define CAN_RX_ELEMENT_R1_RXTS_Pos        0
#define CAN_RX_ELEMENT_R1_RXTS_Msk        (0xFFFFul << CAN_RX_ELEMENT_R1_RXTS_Pos)
#define CAN_RX_ELEMENT_R1_RXTS(value)     ((CAN_RX_ELEMENT_R1_RXTS_Msk & ((value) << CAN_RX_ELEMENT_R1_RXTS_Pos)))
#define CAN_RX_ELEMENT_R1_DLC_Pos         16
#define CAN_RX_ELEMENT_R1_DLC_Msk         (0xFul << CAN_RX_ELEMENT_R1_DLC_Pos)
#define CAN_RX_ELEMENT_R1_DLC(value)      ((CAN_RX_ELEMENT_R1_DLC_Msk & ((value) << CAN_RX_ELEMENT_R1_DLC_Pos)))
#define CAN_RX_ELEMENT_R1_BRS_Pos         20
#define CAN_RX_ELEMENT_R1_BRS             (0x1ul << CAN_RX_ELEMENT_R1_BRS_Pos)
#define CAN_RX_ELEMENT_R1_FDF_Pos         21
#define CAN_RX_ELEMENT_R1_FDF             (0x1ul << CAN_RX_ELEMENT_R1_FDF_Pos)
#define CAN_RX_ELEMENT_R1_FIDX_Pos        24
#define CAN_RX_ELEMENT_R1_FIDX_Msk        (0x7Ful << CAN_RX_ELEMENT_R1_FIDX_Pos)
#define CAN_RX_ELEMENT_R1_FIDX(value)     ((CAN_RX_ELEMENT_R1_FIDX_Msk & ((value) << CAN_RX_ELEMENT_R1_FIDX_Pos)))
#define CAN_RX_ELEMENT_R1_ANMF_Pos        31
#define CAN_RX_ELEMENT_R1_ANMF            (0x1ul << CAN_RX_ELEMENT_R1_ANMF_Pos)

/**
* \brief CAN receive element structure for buffer.
*/
struct can_rx_element_buffer
{
	__IO CAN_RX_ELEMENT_R0_Type R0; ///< CAN Rx Element R0 Configuration
	__IO CAN_RX_ELEMENT_R1_Type R1; ///< CAN Rx Element R1 Configuration
	uint8_t data[CONF_CAN0_ELEMENT_DATA_SIZE]; ///< CAN Rx Data Configuration
};

/**
* \brief CAN receive element structure for FIFO 0.
*/
struct can_rx_element_fifo_0
{
	__IO CAN_RX_ELEMENT_R0_Type R0; ///< CAN Rx Element R0 Configuration
	__IO CAN_RX_ELEMENT_R1_Type R1; ///< CAN Rx Element R1 Configuration
	uint8_t data[CONF_CAN0_ELEMENT_DATA_SIZE]; ///< CAN Rx Data Configuration
};

/**
* \brief CAN receive element structure for FIFO 1.
*/
struct can_rx_element_fifo_1
{
	__IO CAN_RX_ELEMENT_R0_Type R0; ///< CAN Rx Element R0 Configuration
	__IO CAN_RX_ELEMENT_R1_Type R1; ///< CAN Rx Element R1 Configuration
	uint8_t data[CONF_CAN0_ELEMENT_DATA_SIZE]; ///< CAN Rx Data Configuration
};

/* -------- CAN_TX_ELEMENT_T0 : (CAN TX element: 0x00) (R/W 32) Tx Element T0 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t ID: 29;           /*!< bit:  0..28  Identifier */
		uint32_t RTR: 1;           /*!< bit:  29     Remote Transmission Request */
		uint32_t XTD: 1;           /*!< bit:  30     Extended Identifier */
		uint32_t ESI: 1;           /*!< bit:  31     Error State Indicator */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_TX_ELEMENT_T0_Type;

#define CAN_TX_ELEMENT_T0_EXTENDED_ID_Pos          0
#define CAN_TX_ELEMENT_T0_EXTENDED_ID_Msk          (0x1FFFFFFFul << CAN_TX_ELEMENT_T0_EXTENDED_ID_Pos)
#define CAN_TX_ELEMENT_T0_EXTENDED_ID(value)       ((CAN_TX_ELEMENT_T0_EXTENDED_ID_Msk & ((value) << CAN_TX_ELEMENT_T0_EXTENDED_ID_Pos)))
#define CAN_TX_ELEMENT_T0_STANDARD_ID_Pos          18
#define CAN_TX_ELEMENT_T0_STANDARD_ID_Msk          (0x7FFul << CAN_TX_ELEMENT_T0_STANDARD_ID_Pos)
#define CAN_TX_ELEMENT_T0_STANDARD_ID(value)       ((CAN_TX_ELEMENT_T0_STANDARD_ID_Msk & ((value) << CAN_TX_ELEMENT_T0_STANDARD_ID_Pos)))
#define CAN_TX_ELEMENT_T0_RTR_Pos         29
#define CAN_TX_ELEMENT_T0_RTR             (0x1ul << CAN_TX_ELEMENT_T0_RTR_Pos)
#define CAN_TX_ELEMENT_T0_XTD_Pos         30
#define CAN_TX_ELEMENT_T0_XTD             (0x1ul << CAN_TX_ELEMENT_T0_XTD_Pos)
#define CAN_TX_ELEMENT_T0_ESI_Pos         31
#define CAN_TX_ELEMENT_T0_ESI             (0x1ul << CAN_TX_ELEMENT_T0_ESI_Pos)

/* -------- CAN_TX_ELEMENT_T1 : (CAN TX element: 0x01) (R/W 32) Tx Element T1 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t : 16;             /*!< bit: 0..15   Reserved */
		uint32_t DLC: 4;           /*!< bit: 16..19  Data Length Code */
		uint32_t BRS: 1;           /*!< bit: 20      Bit Rate Switch */
		uint32_t FDF: 1;           /*!< bit: 21      FD Format */
		uint32_t : 1;              /*!< bit: 22      Reserved */
		uint32_t EFC: 1;           /*!< bit: 23      Event FIFO Control */
		uint32_t MM: 8;            /*!< bit: 24..31  Message Marker */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_TX_ELEMENT_T1_Type;

#define CAN_TX_ELEMENT_T1_DLC_Pos         16
#define CAN_TX_ELEMENT_T1_DLC_Msk         (0xFul << CAN_TX_ELEMENT_T1_DLC_Pos)
#define CAN_TX_ELEMENT_T1_DLC(value)      ((CAN_TX_ELEMENT_T1_DLC_Msk & ((value) << CAN_TX_ELEMENT_T1_DLC_Pos)))
#define   CAN_TX_ELEMENT_T1_DLC_DATA8_Val        0x8ul  /**< \brief (CAN_RXESC) 8 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA12_Val       0x9ul  /**< \brief (CAN_RXESC) 12 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA16_Val       0xAul  /**< \brief (CAN_RXESC) 16 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA20_Val       0xBul  /**< \brief (CAN_RXESC) 20 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA24_Val       0xCul  /**< \brief (CAN_RXESC) 24 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA32_Val       0xDul  /**< \brief (CAN_RXESC) 32 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA48_Val       0xEul  /**< \brief (CAN_RXESC) 48 byte data field */
#define   CAN_TX_ELEMENT_T1_DLC_DATA64_Val       0xFul  /**< \brief (CAN_RXESC) 64 byte data field */
#define CAN_TX_ELEMENT_T1_BRS_Pos         20
#define CAN_TX_ELEMENT_T1_BRS             (0x1ul << CAN_TX_ELEMENT_T1_BRS_Pos)
#define CAN_TX_ELEMENT_T1_FDF_Pos         21
#define CAN_TX_ELEMENT_T1_FDF             (0x1ul << CAN_TX_ELEMENT_T1_FDF_Pos)
#define CAN_TX_ELEMENT_T1_EFC_Pos         23
#define CAN_TX_ELEMENT_T1_EFC             (0x1ul << CAN_TX_ELEMENT_T1_EFC_Pos)
#define CAN_TX_ELEMENT_T1_MM_Pos          24
#define CAN_TX_ELEMENT_T1_MM_Msk          (0xFFul << CAN_TX_ELEMENT_T1_MM_Pos)
#define CAN_TX_ELEMENT_T1_MM(value)       ((CAN_TX_ELEMENT_T1_MM_Msk & ((value) << CAN_TX_ELEMENT_T1_MM_Pos)))


/*brief CAN transfer element structure.
*
*  Common element structure for transfer buffer and FIFO/QUEUE.
*/
struct can_tx_element
{
	__IO CAN_TX_ELEMENT_T0_Type T0; ///< CAN Tx Element T0 Configuration
	__IO CAN_TX_ELEMENT_T1_Type T1; ///< CAN Tx Element T1 Configuration
	uint8_t data[CONF_CAN0_ELEMENT_DATA_SIZE]; ///< CAN Tx Data Configuration
};


/* -------- CAN_TX_EVENT_ELEMENT_E0 : (CAN TX event element: 0x00) (R/W 32) Tx Event Element E0 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t ID: 29;           /*!< bit: 0..28  Identifier */
		uint32_t RTR: 1;           /*!< bit: 29     Remote Transmission Request */
		uint32_t XTD: 1;           /*!< bit: 30     Extended Identifier */
		uint32_t ESI: 1;           /*!< bit: 31     Error State Indicator */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_TX_EVENT_ELEMENT_E0_Type;

#define CAN_TX_EVENT_ELEMENT_E0_ID_Pos          0
#define CAN_TX_EVENT_ELEMENT_E0_ID_Msk          (0x1FFFFFFFul << CAN_TX_EVENT_ELEMENT_E0_ID_Pos)
#define CAN_TX_EVENT_ELEMENT_E0_ID(value)       ((CAN_TX_EVENT_ELEMENT_E0_ID_Msk & ((value) << CAN_TX_EVENT_ELEMENT_E0_ID_Pos)))
#define CAN_TX_EVENT_ELEMENT_E0_RTR_Pos         29
#define CAN_TX_EVENT_ELEMENT_E0_RTR             (0x1ul << CAN_TX_EVENT_ELEMENT_E0_RTR_Pos)
#define CAN_TX_EVENT_ELEMENT_E0_XTD_Pos         30
#define CAN_TX_EVENT_ELEMENT_E0_XTD             (0x1ul << CAN_TX_EVENT_ELEMENT_E0_XTD_Pos)
#define CAN_TX_EVENT_ELEMENT_E0_ESI_Pos         31
#define CAN_TX_EVENT_ELEMENT_E0_ESI             (0x1ul << CAN_TX_EVENT_ELEMENT_E0_ESI_Pos)


/* -------- CAN_TX_EVENT_ELEMENT_E1 : (CAN TX event element: 0x01) (R/W 32) Tx Event Element E1 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t TXTS: 16;         /*!< bit: 0..15   Tx Timestamp */
		uint32_t DLC: 4;           /*!< bit: 16..19  Data Length Code */
		uint32_t BRS: 1;           /*!< bit: 20      Bit Rate Switch */
		uint32_t FDF: 1;           /*!< bit: 21     e FD Format */
		uint32_t ET: 2;            /*!< bit: 22..23  Event Type */
		uint32_t MM: 8;            /*!< bit: 24..31  Message Marker */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_TX_EVENT_ELEMENT_E1_Type;

#define CAN_TX_EVENT_ELEMENT_E1_TXTS_Pos        0
#define CAN_TX_EVENT_ELEMENT_E1_TXTS_Msk        (0xFFFFul << CAN_TX_EVENT_ELEMENT_E1_TXTS_Pos)
#define CAN_TX_EVENT_ELEMENT_E1_TXTS(value)     ((CAN_TX_EVENT_ELEMENT_E1_TXTS_Msk & ((value) << CAN_TX_EVENT_ELEMENT_E1_TXTS_Pos)))
#define CAN_TX_EVENT_ELEMENT_E1_DLC_Pos         16
#define CAN_TX_EVENT_ELEMENT_E1_DLC_Msk         (0xFul << CAN_TX_EVENT_ELEMENT_E1_DLC_Pos)
#define CAN_TX_EVENT_ELEMENT_E1_DLC(value)      ((CAN_TX_EVENT_ELEMENT_E1_DLC_Msk & ((value) << CAN_TX_EVENT_ELEMENT_E1_DLC_Pos)))
#define CAN_TX_EVENT_ELEMENT_E1_BRS_Pos         20
#define CAN_TX_EVENT_ELEMENT_E1_BRS             (0x1ul << CAN_TX_EVENT_ELEMENT_E1_BRS_Pos)
#define CAN_TX_EVENT_ELEMENT_E1_FDF_Pos         21
#define CAN_TX_EVENT_ELEMENT_E1_FDF             (0x1ul << CAN_TX_EVENT_ELEMENT_E1_FDF_Pos)
#define CAN_TX_EVENT_ELEMENT_E1_ET_Pos          22
#define CAN_TX_EVENT_ELEMENT_E1_ET_Msk          (0x3ul << CAN_TX_EVENT_ELEMENT_E1_ET_Pos)
#define CAN_TX_EVENT_ELEMENT_E1_ET(value)       ((CAN_TX_EVENT_ELEMENT_E1_ET_Msk & ((value) << CAN_TX_EVENT_ELEMENT_E1_ET_Pos)))
#define CAN_TX_EVENT_ELEMENT_E1_MM_Pos          24
#define CAN_TX_EVENT_ELEMENT_E1_MM_Msk          (0xFFul << CAN_TX_EVENT_ELEMENT_E1_MM_Pos)
#define CAN_TX_EVENT_ELEMENT_E1_MM(value)       ((CAN_TX_EVENT_ELEMENT_E1_MM_Msk & ((value) << CAN_TX_EVENT_ELEMENT_E1_MM_Pos)))


/* brief CAN transfer event  FIFO element structure.
*
*  Common element structure for transfer event FIFO.
*/
struct can_tx_event_element
{
	__IO CAN_TX_EVENT_ELEMENT_E0_Type E0; ///< CAN Tx Event Element E0 Configuration
	__IO CAN_TX_EVENT_ELEMENT_E1_Type E1; ///< CAN Tx Event Element E1 Configuration
};

/* -------- CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0 : (CAN standard message ID filter element: 0x00) (R/W 32) Standard Message ID Filter Element S0 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t SFID2: 11;        /*!< bit: 0..10   Standard Filter ID 2 */
		uint32_t : 5;              /*!< bit: 11..15  Reserved */
		uint32_t SFID1: 11;        /*!< bit: 16..26  Standard Filter ID 1 */
		uint32_t SFEC: 3;          /*!< bit: 27..29  Standard Filter Element Configuration */
		uint32_t SFT: 2;           /*!< bit: 30..31  Standard Filter Type */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_Type;

#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2_Pos          0
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2_Msk          (0x7FFul << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2_Pos)
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2(value)       ((CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2_Msk & ((value) << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2_Pos)))
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1_Pos          16
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1_Msk          (0x7FFul << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1_Pos)
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1(value)       ((CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1_Msk & ((value) << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1_Pos)))
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_Pos           27
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_Msk           (0x7ul << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_Pos)
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC(value)        ((CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_Msk & ((value) << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_Pos)))
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_DISABLE_Val     0
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STF0M_Val       1
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STF1M_Val       2
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_REJECT_Val      3
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_PRIORITY_Val    4
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_PRIF0M_Val      5
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_PRIF1M_Val      6
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STRXBUF_Val     7
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_Pos            30
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_Msk            (0x3ul << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_Pos)
#define CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT(value)         ((CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_Msk & ((value) << CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_Pos)))
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_RANGE          CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT(0)
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_DUAL           CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT(1)
#define   CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_CLASSIC        CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT(2)

/**
* \brief CAN standard message ID filter element structure.
*
*  Common element structure for standard message ID filter element.
*/
struct can_standard_message_filter_element
{
	__IO CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_Type S0; ///< CAN Standard Message ID Filter Element S0 Configuration
};

/* -------- CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0 : (CAN extended message ID filter element: 0x00) (R/W 32) Extended Message ID Filter Element F0 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t EFID1: 29;        /*!< bit: 0..28   Extended Filter ID 1 */
		uint32_t EFEC: 3;          /*!< bit: 29..31  Extended Filter Element Configuration */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_Type;

#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFID1_Pos          0
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFID1_Msk          (0x1FFFFFFFul << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFID1_Pos)
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFID1(value)       ((CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFID1_Msk & ((value) << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFID1_Pos)))
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_Pos           29
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_Msk           (0x7ul << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_Pos)
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC(value)        ((CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_Msk & ((value) << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_Pos)))
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_DISABLE_Val       0
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_STF0M_Val         1
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_STF1M_Val         2
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_REJECT_Val        3
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_PRIORITY_Val      4
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_PRIF0M_Val        5
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_PRIF1M_Val        6
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_EFEC_STRXBUF_Val       7

/* -------- CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1 : (CAN extended message ID filter element: 0x01) (R/W 32) Extended Message ID Filter Element F1 Configuration -------- */
typedef union
{
	struct
	{
		uint32_t EFID2: 29;        /*!< bit: 0..28  Extended Filter ID 2 */
		uint32_t : 1;              /*!< bit: 29     Reserved */
		uint32_t EFT: 2;           /*!< bit: 30..31 Extended Filter Type */
	} bit;                       /*!< Structure used for bit  access */
	uint32_t reg;                /*!< Type used for register access */
} CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_Type;

#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFID2_Pos          0
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFID2_Msk          (0x1FFFFFFFul << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFID2_Pos)
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFID2(value)       ((CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFID2_Msk & ((value) << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFID2_Pos)))
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_Pos            30
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_Msk            (0x3ul << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_Pos)
#define CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT(value)         ((CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_Msk & ((value) << CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_Pos)))
#define   CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_RANGEM       CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT(0)
#define   CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_DUAL         CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT(1)
#define   CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_CLASSIC      CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT(2)
#define   CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT_RANGE        CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_EFT(3)

/**
* \brief CAN extended message ID filter element structure.
*
*  Common element structure for extended message ID filter element.
*/
struct can_extended_message_filter_element
{
	__IO CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F0_Type F0; ///< CAN Extended Message ID Filter Element F0 Configuration
	__IO CAN_EXTENDED_MESSAGE_FILTER_ELEMENT_F1_Type F1; ///< CAN Extended Message ID Filter Element F1 Configuration
};

/**
 * \brief Can time out modes.
 */
enum can_timeout_mode
{
	/** Continuous operation. */
	CAN_TIMEOUT_CONTINUES = CAN_TOCC_TOS_CONT,
	/** Timeout controlled by TX Event FIFO. */
	CAN_TIMEOUT_TX_EVEN_FIFO = CAN_TOCC_TOS_TXEF,
	/** Timeout controlled by Rx FIFO 0. */
	CAN_TIMEOUT_RX_FIFO_0 = CAN_TOCC_TOS_RXF0,
	/** Timeout controlled by Rx FIFO 1. */
	CAN_TIMEOUT_RX_FIFO_1 = CAN_TOCC_TOS_RXF1,
};

/**
 * \brief Can nonmatching frames action.
 */
enum can_nonmatching_frames_action
{
	/** Accept in Rx FIFO 0. */
	CAN_NONMATCHING_FRAMES_FIFO_0,
	/** Accept in Rx FIFO 1. */
	CAN_NONMATCHING_FRAMES_FIFO_1,
	/** Reject. */
	CAN_NONMATCHING_FRAMES_REJECT,
};

// Inline functions

/**
 * \brief Get the default transfer buffer element.
 *
 * The default configuration is as follows:
 *  \li 11-bit standard identifier
 *  \li Transmit data frame
 *  \li ID = 0x0ul
 *  \li Store Tx events
 *  \li Frame transmitted in Classic CAN format
 *  \li Data Length Code is 8
 *
 * \param[out] tx_element  Pointer to transfer element struct to Initialise to default values
 */
static inline void can_get_tx_buffer_element_defaults (
   struct can_tx_element *tx_element )
{
	tx_element->T0.reg = 0;
	tx_element->T1.reg = CAN_TX_ELEMENT_T1_EFC |
	                     CAN_TX_ELEMENT_T1_DLC ( CAN_TX_ELEMENT_T1_DLC_DATA8_Val );
}

/**
 * \brief Get the standard message filter default value.
 *
 * The default configuration is as follows:
 *  \li Classic filter: SFID1 = filter, SFID2 = mask
 *  \li Store in Rx FIFO 0 if filter matches
 *  \li SFID2 = 0x7FFul
 *  \li SFID1 = 0x0ul
 *
 * \param[out] sd_filter  Pointer to standard filter element struct to Initialise to default values
 */
static inline void can_get_standard_message_filter_element_default (
   struct can_standard_message_filter_element *sd_filter )
{
	sd_filter->S0.reg = CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID2_Msk |
	                    CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFID1 ( 0 ) |
	                    CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC (
	                       CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STF0M_Val ) |
	                    CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_CLASSIC;
}


/////////////////////////////////////////////////////////////////////////////
///
/// CAN0_FIFO_Length - Function to return length of CAN FIFO Queue
///
/// @return Length of CAN FIFO Queue
///
/////////////////////////////////////////////////////////////////////////////

static inline uint8_t CAN0_FIFO_Length ( void )
{
	return ( uint8_t ) ( REG_CAN0_RXF0S & 0xFF );
}


// Function prototypes

void CAN0_Init ( void );
void CAN0_Message_Tx ( struct can_tx_element *tx_element );
void CAN0_Set_Rx_Filter ( uint32_t, struct can_standard_message_filter_element *sd_filter );
void CAN0_Message_Rx ( struct can_rx_element_fifo_0 *rx_element );
void CAN0_Message_Rx ( struct can_rx_element_fifo_0 *rx_element );
