/////////////////////////////////////////////////////////////////////////////
///
/// @file CAN0_Drivers.cpp
///
/// @brief File containing functions for the CAN Module which enable CAN
/// messages to be transmitted via a buffer, receive filters to be defined,
/// and CAN messages to be received via a FIFO buffer.  These functions are
/// specific to this micro-controller family.
///
/// @copyright Confidential.  Copyright (C) 2020-2021, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"
#include "Application.h"
#include "CAN0_Drivers.h"
#include <string.h>


/////////////////////////////////////////////////////////////////////////////
//
// CAN message ram definition
//
/////////////////////////////////////////////////////////////////////////////

__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_rx_element_buffer can0_rx_buffer[CONF_CAN0_RX_BUFFER_NUM]; ///< CAN0 Receive Buffer
__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_rx_element_fifo_0 can0_rx_fifo_0[CONF_CAN0_RX_FIFO_0_NUM];
__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_rx_element_fifo_1 can0_rx_fifo_1[CONF_CAN0_RX_FIFO_1_NUM];
__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_tx_element can0_tx_buffer[CONF_CAN0_TX_BUFFER_NUM + CONF_CAN0_TX_FIFO_QUEUE_NUM];
__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_tx_event_element can0_tx_event_fifo[CONF_CAN0_TX_EVENT_FIFO];
__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_standard_message_filter_element can0_rx_standard_filter[CONF_CAN0_RX_STANDARD_ID_FILTER_NUM];
__attribute__ ( ( __aligned__ ( 4 ) ) )
static struct can_extended_message_filter_element can0_rx_extended_filter[CONF_CAN0_RX_EXTENDED_ID_FILTER_NUM];

//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_rx_element_buffer can1_rx_buffer[CONF_CAN1_RX_BUFFER_NUM];
//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_rx_element_fifo_0 can1_rx_fifo_0[CONF_CAN1_RX_FIFO_0_NUM];
//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_rx_element_fifo_1 can1_rx_fifo_1[CONF_CAN1_RX_FIFO_1_NUM];
//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_tx_element can1_tx_buffer[CONF_CAN1_TX_BUFFER_NUM + CONF_CAN1_TX_FIFO_QUEUE_NUM];
//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_tx_event_element can1_tx_event_fifo[CONF_CAN1_TX_EVENT_FIFO];
//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_standard_message_filter_element can1_rx_standard_filter[CONF_CAN1_RX_STANDARD_ID_FILTER_NUM];
//__attribute__ ( ( __aligned__ ( 4 ) ) )
//static struct can_extended_message_filter_element can1_rx_extended_filter[CONF_CAN1_RX_EXTENDED_ID_FILTER_NUM];


/////////////////////////////////////////////////////////////////////////////
///
/// CAN0_Init - Function that initializes the CAN Data Bus
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void CAN0_Init ( void )
{
	/// @module CAN0 : Configure as follows:
	/// + GCLK generator 0 (GCLK main) clock source
	/// + Not run in standby mode
	/// + Disable Watchdog
	/// + Transmit pause enabled
	/// + Edge filtering during bus integration enabled
	/// + Protocol exception handling enabled
	/// + Automatic retransmission enabled
	/// + Clock stop request disabled
	/// + Clock stop acknowledge disabled
	/// + Timestamp Counter Prescaler 1
	/// + Timeout Period with 0xFFFF
	/// + Timeout Mode: Continuous operation
	/// + Disable Timeout
	/// + Transmitter Delay Compensation Offset is 0
	/// + Transmitter Delay Compensation Filter Window Length is 0
	/// + Reject non-matching standard frames
	/// + Reject non-matching extended frames
	/// + Reject remote standard frames
	/// + Reject remote extended frames
	/// + Extended ID Mask is 0x1FFFFFFF
	/// + Rx FIFO 0 Operation Mode: overwrite
	/// + Disable Rx FIFO 0 Watermark
	/// + Rx FIFO 1 Operation Mode: overwrite
	/// + Disable Rx FIFO 1 Watermark
	/// + Tx FIFO/Queue Mode: FIFO
	/// + Disable Tx Event FIFO Watermark

	/// Configuration steps:
	/// + Enable configuration changes to CAN0
	REG_CAN0_CCCR |= CAN_CCCR_CCE;

	/// + Configure message memory
	REG_CAN0_SIDFC = CAN_SIDFC_FLSSA ( ( uint32_t ) can0_rx_standard_filter ) |
	                 CAN_SIDFC_LSS ( CONF_CAN0_RX_STANDARD_ID_FILTER_NUM );
	REG_CAN0_XIDFC = CAN_XIDFC_FLESA ( ( uint32_t ) can0_rx_extended_filter ) |
	                 CAN_XIDFC_LSE ( CONF_CAN0_RX_EXTENDED_ID_FILTER_NUM );
	REG_CAN0_RXF0C = CAN_RXF0C_F0SA ( ( uint32_t ) can0_rx_fifo_0 ) |
	                 CAN_RXF0C_F0S ( CONF_CAN0_RX_FIFO_0_NUM );
	REG_CAN0_RXF1C = CAN_RXF1C_F1SA ( ( uint32_t ) can0_rx_fifo_1 ) |
	                 CAN_RXF1C_F1S ( CONF_CAN0_RX_FIFO_1_NUM );
	REG_CAN0_RXBC = CAN_RXBC_RBSA ( ( uint32_t ) can0_rx_buffer );
	REG_CAN0_TXBC = CAN_TXBC_TBSA ( ( uint32_t ) can0_tx_buffer ) |
	                CAN_TXBC_NDTB ( CONF_CAN0_TX_BUFFER_NUM ) |
	                CAN_TXBC_TFQS ( CONF_CAN0_TX_FIFO_QUEUE_NUM );
	REG_CAN0_TXEFC = CAN_TXEFC_EFSA ( ( uint32_t ) can0_tx_event_fifo ) |
	                 CAN_TXEFC_EFS ( CONF_CAN0_TX_EVENT_FIFO );


	// The data size in conf_can.h should be 8/12/16/20/24/32/48/64,
	// The corresponding setting value in register is 0/1//2/3/4/5/6/7.
	// To simplify the calculation, separate to two group 8/12/16/20/24 which
	// increased with 4 and 32/48/64 which increased with 16.
	if ( CONF_CAN0_ELEMENT_DATA_SIZE <= 24 )
	{
		REG_CAN0_RXESC = CAN_RXESC_RBDS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 8 ) / 4 ) |
		                 CAN_RXESC_F0DS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 8 ) / 4 ) |
		                 CAN_RXESC_F1DS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 8 ) / 4 );
		REG_CAN0_TXESC = CAN_TXESC_TBDS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 8 ) / 4 );
	}
	else
	{
		REG_CAN0_RXESC = CAN_RXESC_RBDS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 32 ) / 16 + 5 ) |
		                 CAN_RXESC_F0DS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 32 ) / 16 + 5 ) |
		                 CAN_RXESC_F1DS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 32 ) / 16 + 5 );
		REG_CAN0_TXESC = CAN_TXESC_TBDS ( ( CONF_CAN0_ELEMENT_DATA_SIZE - 32 ) / 16 + 5 );
	}


	/// + Configure timing setting
	REG_CAN0_NBTP = CAN_NBTP_NBRP ( CONF_CAN_NBTP_NBRP_VALUE ) |
	                CAN_NBTP_NSJW ( CONF_CAN_NBTP_NSJW_VALUE ) |
	                CAN_NBTP_NTSEG1 ( CONF_CAN_NBTP_NTSEG1_VALUE ) |
	                CAN_NBTP_NTSEG2 ( CONF_CAN_NBTP_NTSEG2_VALUE );
	REG_CAN0_DBTP = CAN_DBTP_DBRP ( CONF_CAN_DBTP_DBRP_VALUE ) |
	                CAN_DBTP_DSJW ( CONF_CAN_DBTP_DSJW_VALUE ) |
	                CAN_DBTP_DTSEG1 ( CONF_CAN_DBTP_DTSEG1_VALUE ) |
	                CAN_DBTP_DTSEG2 ( CONF_CAN_DBTP_DTSEG2_VALUE );

	// Configure CAN Watchdog
	REG_CAN0_RWD |= CAN_RWD_WDC ( 0x00 );

	// Configure transmit pause
	REG_CAN0_CCCR |= CAN_CCCR_TXP;

	// Configure edge filtering
	REG_CAN0_CCCR |= CAN_CCCR_EFBI;

	// Configure protocol exception handling
	REG_CAN0_CCCR |= CAN_CCCR_PXHD;

	// Configure timestamp prescaler
	REG_CAN0_TSCC = CAN_TSCC_TCP ( 0 ) |
	                CAN_TSCC_TSS_INC_Val;

	// Configure timeout period
	REG_CAN0_TOCC = CAN_TOCC_TOP ( 0xFFFF ) |
	                CAN_TIMEOUT_CONTINUES;

	// Configure delay compensation offset
	REG_CAN0_TDCR = CAN_TDCR_TDCO ( 0 ) |
	                CAN_TDCR_TDCF ( 0 );

	// Configure non matching frames action
	REG_CAN0_GFC = CAN_GFC_ANFS ( CAN_NONMATCHING_FRAMES_REJECT ) |
	               CAN_GFC_ANFE ( CAN_NONMATCHING_FRAMES_REJECT );

	// Configure remote frames standard reject
	REG_CAN0_GFC |= CAN_GFC_RRFS;

	// Configure remote frames extended reject
	REG_CAN0_GFC |= CAN_GFC_RRFE;

	// Configure extended ID mask
	REG_CAN0_XIDAM = 0x1FFFFFFF;

	// Configure Rx FIFO0 overwrite
	REG_CAN0_RXF0C |= CAN_RXF0C_F0OM;

	// Configure Rx FIFO0 watermark
	REG_CAN0_RXF0C |= CAN_RXF0C_F0WM ( 0 );

	// Configure Rx FIFO1 overwrite
	REG_CAN0_RXF1C |= CAN_RXF1C_F1OM;

	// Configure Rx FIFO0 watermark
	REG_CAN0_RXF1C |= CAN_RXF1C_F1WM ( 0 );

	// Configure Tx FIFO watermark
	REG_CAN0_TXEFC |= CAN_TXEFC_EFWM ( 0 );

	// Enable the interrupt setting which no need change
	REG_CAN0_ILE = CAN_ILE_EINT0 | CAN_ILE_EINT1;
	REG_CAN0_TXBTIE = CAN_TXBTIE_MASK;
	REG_CAN0_TXBCIE = CAN_TXBCIE_MASK;


	/// + CAN start and wait for sync
	REG_CAN0_CCCR &= ~CAN_CCCR_INIT;
	while ( REG_CAN0_CCCR & CAN_CCCR_INIT );


	/// + Enable CAN interrupts for data and arbitration errors, and new
	/// FIFO0 message
	REG_CAN0_ILE = CAN_ILE_EINT0;
	REG_CAN0_IE = CAN_IE_PEAE
	              | CAN_IE_PEDE
	              | CAN_IE_RF0NE;
	NVIC_EnableIRQ ( CAN0_IRQn );
}


/////////////////////////////////////////////////////////////////////////////
///
/// CAN0_Message_Tx - Function that sends a CAN message
///
/// @param [in] tx_element - Pointer to CAN message structure to be
/// transmitted
///
/////////////////////////////////////////////////////////////////////////////

#define CAN0_TX_BUFFER_INDEX 0

void CAN0_Message_Tx ( struct can_tx_element *tx_element )
{
	unsigned int i; ///< Loop counter

	// Set Function Test Pin if enabled
	#ifdef CAN0_Message_Tx_FUNCTION
	Function_Timer_Pin_Set();
	#endif

	// Copy message across to buffer
	can0_tx_buffer[CAN0_TX_BUFFER_INDEX].T0.reg = tx_element->T0.reg;
	can0_tx_buffer[CAN0_TX_BUFFER_INDEX].T1.reg = tx_element->T1.reg;
	for ( i = 0; i < CONF_CAN0_ELEMENT_DATA_SIZE; i++ )
	{
		can0_tx_buffer[CAN0_TX_BUFFER_INDEX].data[i] = tx_element->data[i];
	}

	// When transmitter is ready send
	while ( REG_CAN0_CCCR & CAN_CCCR_CCE );
	REG_CAN0_TXBAR = 1 << CAN0_TX_BUFFER_INDEX;

	// Clear Function Test Pin if enabled
	#ifdef CAN0_Message_Tx_FUNCTION
	Function_Timer_Pin_Clear();
	#endif
}


/////////////////////////////////////////////////////////////////////////////
///
/// CAN0_Set_Rx_Filter - Function that sends a CAN message
///
/// @param [in] Index - Storage index for filter
/// @param [in] sd_filter - Pointer to CAN filter structure to be stored
///
/////////////////////////////////////////////////////////////////////////////

void CAN0_Set_Rx_Filter ( uint32_t Index, struct can_standard_message_filter_element *sd_filter )
{
	can0_rx_standard_filter[Index].S0.reg = sd_filter->S0.reg;
}


/////////////////////////////////////////////////////////////////////////////
///
/// CAN0_Message_Rx - Function that receives a CAN message
///
/// @param [in] rx_element - Pointer to CAN message structure to be received
///
/////////////////////////////////////////////////////////////////////////////

void CAN0_Message_Rx ( struct can_rx_element_fifo_0 *rx_element )
{
	/// Variable which contains CAN FIFO0 index
	static volatile uint32_t standard_receive_index = 0;

	// Copy data across
	memcpy ( rx_element, &can0_rx_fifo_0[standard_receive_index], sizeof ( struct can_rx_element_buffer ) );

	// Acknowledge FIFO pull
	REG_CAN0_RXF0A = CAN_RXF0A_F0AI ( standard_receive_index );

	// Calculate new FIFO index
	standard_receive_index = standard_receive_index + 1;
	if ( standard_receive_index == CONF_CAN0_RX_FIFO_0_NUM )
	{
		standard_receive_index = 0;
	}
}