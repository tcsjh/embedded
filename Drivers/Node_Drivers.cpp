//////////////////////////////////////////////////////////////////////////////
///
/// @file Node_Drivers.cpp
///
/// @brief File containing functions for accessing the test and CAN pins, and
/// the HRT and delay timers.  These functions are specific to this
/// application and Circuit Card Assembly.
///
/// @copyright Confidential.  Copyright (C) 2020-2021, Chris Wheater.
/// All rights reserved.
///
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
//////////////////////////////////////////////////////////////////////////////

#include "sam.h"
#include "Node_Drivers.h"
#include "Application.h"
#include "CLK_Drivers.h"


//////////////////////////////////////////////////////////////////////////////
///
/// Test_Pin_Init - Function that Initialises the test pins
///
//////////////////////////////////////////////////////////////////////////////

void Test_Pin_Init ( void )
{
	/// @module PORT : Configure GPIO Outputs
	REG_PORT_DIRSET0 = SYNC_EVENT_PIN_MASK
	| ASYNC_EVENT_PIN_MASK
	| SRT_LOOP_PIN_MASK
	| FUNCTION_TIMER_PIN_MASK
	| CAN0_YELLOW_LED_PIN_MASK
	| CAN0_RED_LED_PIN_MASK;

	REG_PORT_DIRSET1 = CAN0_GREEN_LED_PIN_MASK;
}


//////////////////////////////////////////////////////////////////////////////
///
/// CAN_Pin_Init - Function that Initialises the CAN pins
///
//////////////////////////////////////////////////////////////////////////////

void CAN_Pin_Init ( void )
{
		/// @module PORT : Configure pin allocations for CAN0 data bus
		REG_PORT_WRCONFIG0 = PORT_WRCONFIG_HWSEL
		| PORT_WRCONFIG_WRPINCFG
		| PORT_WRCONFIG_WRPMUX
		| PORT_WRCONFIG_PMUX ( 0x6 )
		| PORT_WRCONFIG_PMUXEN
		| ( ( CAN0_TX_MASK
		| CAN0_RX_MASK ) >> 16 );

		/// @module MCLK : Enable Master Clock to CAN0
		REG_MCLK_AHBMASK = REG_MCLK_AHBMASK
		| MCLK_AHBMASK_CAN0;

		/// @module GCLK : Enable Peripheral Clock Channel for CAN0 from GCLK0
		REG_GCLK_PCHCTRL26 = GCLK_PCHCTRL_CHEN
		| GCLK_PCHCTRL_GEN_GCLK0;
}


/////////////////////////////////////////////////////////////////////////////
///
/// TC0_Init - Function that initializes TC0 to generate synchronous
/// interrupts
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void TC0_Init ( void )
{
	/// + @module MCLK : Enable Master Clock to TC0
	REG_MCLK_APBCMASK = REG_MCLK_APBCMASK
	| MCLK_APBCMASK_TC0;

	/// + @module GCLK : Enable peripheral clock for TC0 and TC1 from GCLK0
	REG_GCLK_PCHCTRL30 = GCLK_PCHCTRL_CHEN
	| GCLK_PCHCTRL_GEN_GCLK0;

	/// + @module TC0 : Configure into Match Frequency Waveform Mode, with a
	/// comparison value set to provide required TC0 interrupt frequency
	REG_TC0_WAVE = TC_WAVE_WAVEGEN_MFRQ;
	REG_TC0_COUNT16_CC0 = GCLK0_FREQ_KHZ / TC0_INTERRUPT_FREQ_KHZ;
	REG_TC0_CTRLA = TC_CTRLA_ENABLE;

	/// + @module TC0 : Enable TC0 Match or Capture Interrupts
	REG_TC0_INTENSET = TC_INTENSET_MC0;
	NVIC_EnableIRQ ( TC0_IRQn );
}


/////////////////////////////////////////////////////////////////////////////
//
// Macros which are private to functions within this file
//
/////////////////////////////////////////////////////////////////////////////

/// Delay_us - Timing data
#define MINIMUM_DELAY 12 ///< Minimum effective delay time (including call times)


/////////////////////////////////////////////////////////////////////////////
///
/// Delay_Init - Function that initializes TC1 for use as a delay counter
///
/// @module MCLK : Enabling of Peripheral Clock Channel to TC1
/// @module GCLK : Enabling of Peripheral Clock Channel to TC0 and TC1 from
/// Generic Clock Generator 0 (48MHz)
/// @module TC0 : Enabling of Peripheral Clock Channel (48MHz)
/// @module TC1 : Enabling of Peripheral Clock Channel (48MHz)
/// @module TC1 : Configure and enable TC1 with DIV 4 prescaler
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void Delay_Init ( void )
{
	/// + Enable Peripheral Clock to Timer Counter Module 1 (TC1) (48MHz)
	REG_MCLK_APBCMASK = REG_MCLK_APBCMASK
	| MCLK_APBCMASK_TC1;

	/// + Configure Peripheral Channel Control for Timer Counters TC0 and TC1
	/// to source clock from Generic Clock Controller 0 (48MHz)
	REG_GCLK_PCHCTRL30 = GCLK_PCHCTRL_CHEN
	| GCLK_PCHCTRL_GEN_GCLK0;

	/// + Configure Timer Counter 1 (TC1) with a DIV4 prescaler,
	/// which will cause a count every 1/12uS.
	REG_TC1_CTRLA = TC_CTRLA_ENABLE
	| TC_CTRLA_PRESCALER_DIV4;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Delay_us - Function that causes a delay in microseconds.  During the delay
/// period, other HRT functions which are called by interrupts will continue
/// to run.  The delay period includes the time to call the routine.  The
/// minimum effective delay is held within MINIMUM_DELAY.  Calls of less than
/// this time will be less as they will be only be the stack call time.
///
/// @param[in] Delay - Period in microseconds for the required delay (max 1000)
///
/// @module TC1 : Zero Timer Counter 1
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void Delay_us ( unsigned int Delay )
{
	unsigned int TC1_Count; // Variable for holding TC1 Counter value

	/// + Zero Timer Counter 1
	REG_TC1_COUNT16_COUNT = 0;

	/// + Wait for counter to increase to required delay period
	do
	{
		REG_TC1_CTRLBSET = TC_CTRLBCLR_CMD_READSYNC;

		while ( REG_TC1_SYNCBUSY & TC_SYNCBUSY_CTRLB );
		while ( REG_TC1_SYNCBUSY & TC_SYNCBUSY_COUNT );
		TC1_Count = REG_TC1_COUNT16_COUNT;
	}
	while ( TC1_Count <= 12 * Delay );
}


/////////////////////////////////////////////////////////////////////////////
///
/// Delay_ms - Function that causes a delay in milliseconds.  During the
/// delay period, other HRT functions which are called by interrupts will
/// continue to run.
///
/// @param[in] Delay - Period in milliseconds for the required delay
///
/////////////////////////////////////////////////////////////////////////////

void Delay_ms ( unsigned int Delay )
{
	// Loop Delay times
	while ( Delay != 0 )
	{
		Delay_us ( 995 );  // Calibrated at 50mS
		Delay = Delay - 1;
	}
}