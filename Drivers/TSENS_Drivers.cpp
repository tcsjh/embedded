/////////////////////////////////////////////////////////////////////////////
///
/// @file TSENS_Drivers.cpp
///
/// @brief File containing functions and definitions for using the TSENS
/// Module, which are specific to this range of micro controllers.
///
/// @copyright Confidential.  Copyright (C) 2015-2020, by Chris Wheater.
/// All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Include application macros and libraries
//
/////////////////////////////////////////////////////////////////////////////

#include "sam.h"


/////////////////////////////////////////////////////////////////////////////
///
/// TSEN_Init - Initialize and configure TSENS
///
/// @module TSENS : Configure and calibrate TSENS
///
/// Process steps as follows:
///
/////////////////////////////////////////////////////////////////////////////

void TSEN_Init ( void )
{
	/// + @module MCLK : Enable Master Clock to TSENS module
	REG_MCLK_APBAMASK = REG_MCLK_APBAMASK
	                    | MCLK_APBAMASK_TSENS;

	/// + @module GCLK : Enable peripheral clock for TSENS module from GCLK0
	REG_GCLK_PCHCTRL5 = GCLK_PCHCTRL_CHEN
	                    | GCLK_PCHCTRL_GEN_GCLK0;

	/// + @module TSENS : Configure TSENS calibration data
	REG_TSENS_CAL = ( ( * ( uint32_t * ) NVMCTRL_TEMP_LOG & 0x03F ) << 8 )
	                | ( ( * ( uint32_t * ) NVMCTRL_TEMP_LOG & 0xFC0 ) >> 6 );

	REG_TSENS_GAIN = ( * ( uint32_t * ) NVMCTRL_TEMP_LOG & 0xFFFFFF000 ) >> 12
	                 | ( ( * ( uint32_t * ) ( NVMCTRL_TEMP_LOG + 4 ) & 0x7 ) << 29 );

	REG_TSENS_OFFSET = ( ( * ( uint32_t * ) ( NVMCTRL_TEMP_LOG + 4 ) ) >> 4 );

	// + @module TSENS : Compensating for clock speed
	REG_TSENS_GAIN = REG_TSENS_GAIN / 1;

	/// + @module TSENS : Enable Temperature Sensing Module (TSENS)
	REG_TSENS_CTRLA = TSENS_CTRLA_ENABLE;
	while ( REG_TSENS_SYNCBUSY & TSENS_SYNCBUSY_ENABLE );

	/// + @module TSENS : Initiate next measurement
	REG_TSENS_CTRLB = TSENS_CTRLB_START;
}